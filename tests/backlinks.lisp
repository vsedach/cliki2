;;;; Copyright 2023 Vladimir Sedach <vsedach@common-lisp.net>

;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public
;;;; License along with this program. If not, see
;;;; <http://www.gnu.org/licenses/>.

(in-package #:cliki2.tests)

(fiveam:def-suite backlinks-tests :in cliki2-tests)
(fiveam:in-suite backlinks-tests)

(test-str print-links-list-empty
  "<ul></ul>"
  (html-output
   (cliki2::print-link-list () #'princ)))

(test-str print-links-list-stuff
  "<ul><li><a href=\"/one\" class=\"new\">one</a><li><a href=\"/two\" class=\"new\">two</a></ul>"
  (html-output
   (cliki2::print-link-list '("one" "two") #'cliki2::pprint-topic-link)))

(fiveam:test /site/backlinks0
  (let* ((article-title (cliki2::make-random-string 20))
         (article (cliki2::wiki-new
                   'cliki2::article
                   (cliki2::make-article :article-title article-title))))
    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
      (cliki2::add-revision article "Potatoes are delicious" "fact"))
    (let ((page (get-request 'cliki2::/site/backlinks
                             "article" article-title)))
      (fiveam:is (search "<h1>Link information for " page))
      (fiveam:is (search "</h1>
    Topics:" page))
      (fiveam:is (search "Links to other articles:" page))
      (fiveam:is (search "Links from other articles:" page))
      (fiveam:is (search "Articles in " page)))))
