;;;; Copyright 2023 Vladimir Sedach <vsedach@common-lisp.net>

;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public
;;;; License along with this program. If not, see
;;;; <http://www.gnu.org/licenses/>.

(in-package #:cliki2.tests)

(fiveam:def-suite history-tests :in cliki2-tests)
(fiveam:in-suite history-tests)

(test-str output-undo-button1
  "<input type=\"hidden\" name=\"undo-revision\" value=\"3565062000\" />
    (<input type=\"submit\" name=\"undo\" value=\"undo\" class=\"undo\" />)"
  (html-output
    (mock 'cliki2::youre-banned? (constantly nil)
     (cliki2::output-undo-button
      (list "Some Article"
            3565062000
            ""
            nil
            "Contributor"
            "aaaa:aaaa:aaaa:aaaa:aaaa:aaaa:aaaa:aaaa")))))

(test-str output-compare-link1
  "(<a class=\"internal\" href=\"/site/compare-revisions?article=Some%20article&old=3565062000&diff=3565063000\">prev</a>)"
  (html-output
    (cliki2::output-compare-link
     (list "Some article"
           3565062000
           "old revision"
           nil
           "Contributor"
           "aaaa:aaaa:aaaa:aaaa:aaaa:aaaa:aaaa:aaaa")
     (list "Some article"
           3565063000
           "old revision"
           nil
           "Contributor"
           "111.111.111.111")
     "prev")))

(fiveam:test /site/history1
  (let* ((article-title (cliki2::make-random-string 20))
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title))))
    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
     (cliki2::add-revision
      article "Potatoes are delicious" "created page"))
    (fiveam:is
     (cl-ppcre:scan
      (cl-ppcre:create-scanner
"^<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
    <title>Wooki: History of article: \"[0-9a-zA-Z]{20}\"</title>
    <link rel=\"alternate\" type=\"application/atom\\+xml\" title=\"article changes\" href=\"/site/feed/article.atom\\?title=[0-9a-zA-Z]{20}\">
    <link  rel=\"stylesheet\" href=\"/static/css/style.css\">
    <link  rel=\"stylesheet\" href=\"/static/css/colorize.css\">
  </head>

  <body>
    <span class=\"hidden\">Wooki - History of article: \"[0-9a-zA-Z]{20}\"</span>
    <div id=\"content\"><div id=\"content-area\"><h1>History of article <a href=\"/[0-9a-zA-Z]{20}\" class=\"internal\">[0-9a-zA-Z]{20}</a></h1>
    <a class=\"internal\" href=\"/site/feed/article.atom\\?title=[0-9a-zA-Z]{20}\">ATOM feed</a>
    <form method=\"post\" action=\"/site/history-with-undo\">
    <input type=\"hidden\" name=\"article\" value=\"[0-9a-zA-Z]{20}\" />
    <input type=\"submit\" value=\"Compare selected versions\" />
    <table id=\"pagehistory\"><tr><td></td><td><input type=\"radio\" name=\"old\" value=\"[0-9]{10}\" /></td><td><input type=\"radio\" name=\"diff\" value=\"[0-9]{10}\" /></td><td><a class=\"internal\" href=\"/site/view-revision\\?article=[0-9a-zA-Z]{20}&date=[0-9]{10}\">.+ [0-9]{4} .+ GMT</a> <a class=\"internal\" href=\"/site/account\\?name=4.5.6.7\">4.5.6.7</a> \\(<em>created page</em>\\) <input type=\"hidden\" name=\"undo-revision\" value=\"[0-9]{10}\" />
    \\(<input type=\"submit\" name=\"undo\" value=\"undo\" class=\"undo\" />\\)</td></tr></table>
    <input type=\"submit\" value=\"Compare selected versions\" />
    </form></div>
  <div id=\"footer\" class=\"buttonbar\"><ul><li><a href=\"/[0-9a-zA-Z]{20}\">Current version</a></li>
      <li><a href=\"/site/history\\?article=[0-9a-zA-Z]{20}\">History</a></li>
      <li><a href=\"/site/backlinks\\?article=[0-9a-zA-Z]{20}\">Backlinks</a></li><li><a href=\"/site/edit-article\\?title=[0-9a-zA-Z]{20}&amp;from-revision=[0-9]{10}\">Edit</a></li><li><a href=\"/site/edit-article\\?create=t\">Create</a></li></ul></div>
  </div>
  <div id=\"header-buttons\" class=\"buttonbar\">
    <ul>
      <li><a href=\"/\">Home</a></li>
      <li><a href=\"/site/recent-changes\">Recent Changes</a></li>
      <li><a href=\"/Wooki\">About</a></li>
      <li><a href=\"/Text%20Formatting\">Text Formatting</a></li>
      <li><a href=\"/site/tools\">Tools</a></li>
    </ul>
    <div id=\"search\">
      <form action=\"/site/search\">
        <label for=\"search_query\" class=\"hidden\">Search Wooki</label>
        <input type=\"text\" name=\"query\" id=\"search_query\" value=\"\" />
        <input type=\"submit\" value=\"search\" />
      </form>
    </div>
  </div>
  <div id=\"pageheader\">
    <div id=\"header\">
      <span id=\"logo\">Wooki</span>
      <span id=\"slogan\">test wiki description</span>
      <div id=\"login\"><form method=\"post\" action=\"/site/login\">
                 <label for=\"login_name\" class=\"hidden\">Account name</label>
                 <input type=\"text\" name=\"name\" id=\"login_name\" class=\"login_input\" />
                 <label for= \"login_password\" class=\"hidden\">Password</label>
                 <input type=\"password\" name=\"password\" id=\"login_password\" class=\"login_input\" />
                 <input type=\"submit\" name=\"login\" value=\"login\" id=\"login_submit\" /><br />
                 <div id=\"register\"><a href=\"/site/register\">register</a></div>
                 <input type=\"submit\" name=\"reset-pw\" value=\"reset password\" id=\"reset_pw\" />
               </form>
      </div>
    </div>
  </div>
  </body></html>$"
       :single-line-mode t)
      (get-request 'cliki2::/site/history
                   "article" article-title)))))

(fiveam:test /site/history2
  (let* ((article-title (cliki2::make-random-string 20))
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title))))
    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
     (cliki2::add-revision
      article "Potatoes are delicious" "created page")
      (cliki2::add-revision
      article "Potatoes are delicious /(fact)" "first edit"))
    (fiveam:is
     (cl-ppcre:scan
      (cl-ppcre:create-scanner
"^<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
    <title>Wooki: History of article: \"[0-9a-zA-Z]{20}\"</title>
    <link rel=\"alternate\" type=\"application/atom\\+xml\" title=\"article changes\" href=\"/site/feed/article.atom\\?title=[0-9a-zA-Z]{20}\">
    <link  rel=\"stylesheet\" href=\"/static/css/style.css\">
    <link  rel=\"stylesheet\" href=\"/static/css/colorize.css\">
  </head>

  <body>
    <span class=\"hidden\">Wooki - History of article: \"[0-9a-zA-Z]{20}\"</span>
    <div id=\"content\"><div id=\"content-area\"><h1>History of article <a href=\"/[0-9a-zA-Z]{20}\" class=\"internal\">[0-9a-zA-Z]{20}</a></h1>
    <a class=\"internal\" href=\"/site/feed/article.atom\\?title=[0-9a-zA-Z]{20}\">ATOM feed</a>
    <form method=\"post\" action=\"/site/history-with-undo\">
    <input type=\"hidden\" name=\"article\" value=\"[0-9a-zA-Z]{20}\" />
    <input type=\"submit\" value=\"Compare selected versions\" />
    <table id=\"pagehistory\"><tr><td>\\(<a class=\"internal\" href=\"/site/compare-revisions\\?article=[0-9a-zA-Z]{20}&old=[0-9]{10}&diff=[0-9]{10}\">prev</a>\\)</td><td><input type=\"radio\" name=\"old\" value=\"[0-9]{10}\" /></td><td><input type=\"radio\" name=\"diff\" value=\"[0-9]{10}\" /></td><td><a class=\"internal\" href=\"/site/view-revision\\?article=[0-9a-zA-Z]{20}&date=[0-9]{10}\">.+ [0-9]{4} .+ GMT</a> <a class=\"internal\" href=\"/site/account\\?name=4.5.6.7\">4.5.6.7</a> \\(<em>first edit</em>\\) <input type=\"hidden\" name=\"undo-revision\" value=\"[0-9]{10}\" />
    \\(<input type=\"submit\" name=\"undo\" value=\"undo\" class=\"undo\" />\\)</td></tr><tr><td></td><td><input type=\"radio\" name=\"old\" value=\"[0-9]{10}\" /></td><td><input type=\"radio\" name=\"diff\" value=\"[0-9]{10}\" /></td><td><a class=\"internal\" href=\"/site/view-revision\\?article=[0-9a-zA-Z]{20}&date=[0-9]{10}\">.+ [0-9]{4} .+ GMT</a> <a class=\"internal\" href=\"/site/account\\?name=4.5.6.7\">4.5.6.7</a> \\(<em>created page</em>\\) </td></tr></table>
    <input type=\"submit\" value=\"Compare selected versions\" />
    </form></div>
  <div id=\"footer\" class=\"buttonbar\"><ul><li><a href=\"/[0-9a-zA-Z]{20}\">Current version</a></li>
      <li><a href=\"/site/history\\?article=[0-9a-zA-Z]{20}\">History</a></li>
      <li><a href=\"/site/backlinks\\?article=[0-9a-zA-Z]{20}\">Backlinks</a></li><li><a href=\"/site/edit-article\\?title=[0-9a-zA-Z]{20}&amp;from-revision=[0-9]{10}\">Edit</a></li><li><a href=\"/site/edit-article\\?create=t\">Create</a></li></ul></div>
  </div>
  <div id=\"header-buttons\" class=\"buttonbar\">
    <ul>
      <li><a href=\"/\">Home</a></li>
      <li><a href=\"/site/recent-changes\">Recent Changes</a></li>
      <li><a href=\"/Wooki\">About</a></li>
      <li><a href=\"/Text%20Formatting\">Text Formatting</a></li>
      <li><a href=\"/site/tools\">Tools</a></li>
    </ul>
    <div id=\"search\">
      <form action=\"/site/search\">
        <label for=\"search_query\" class=\"hidden\">Search Wooki</label>
        <input type=\"text\" name=\"query\" id=\"search_query\" value=\"\" />
        <input type=\"submit\" value=\"search\" />
      </form>
    </div>
  </div>
  <div id=\"pageheader\">
    <div id=\"header\">
      <span id=\"logo\">Wooki</span>
      <span id=\"slogan\">test wiki description</span>
      <div id=\"login\"><form method=\"post\" action=\"/site/login\">
                 <label for=\"login_name\" class=\"hidden\">Account name</label>
                 <input type=\"text\" name=\"name\" id=\"login_name\" class=\"login_input\" />
                 <label for= \"login_password\" class=\"hidden\">Password</label>
                 <input type=\"password\" name=\"password\" id=\"login_password\" class=\"login_input\" />
                 <input type=\"submit\" name=\"login\" value=\"login\" id=\"login_submit\" /><br />
                 <div id=\"register\"><a href=\"/site/register\">register</a></div>
                 <input type=\"submit\" name=\"reset-pw\" value=\"reset password\" id=\"reset_pw\" />
               </form>
      </div>
    </div>
  </div>
  </body></html>$"
       :single-line-mode t)
      (get-request 'cliki2::/site/history
                   "article" article-title)))))

(fiveam:test /site/history3
  (let* ((article-title (cliki2::make-random-string 20))
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title))))
    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
      (cliki2::add-revision
       article "Potatoes are delicious" "created page")
      (cliki2::add-revision
       article "Potatoes are delicious /(fact)" "a fact")
      (cliki2::add-revision
       article "Potatoes are delicious *(fact)" "fact markup"))
    (fiveam:is
     (cl-ppcre:scan
      (cl-ppcre:create-scanner
       "^<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
    <title>Wooki: History of article: \"[0-9a-zA-Z]{20}\"</title>
    <link rel=\"alternate\" type=\"application/atom\\+xml\" title=\"article changes\" href=\"/site/feed/article.atom\\?title=[0-9a-zA-Z]{20}\">
    <link  rel=\"stylesheet\" href=\"/static/css/style.css\">
    <link  rel=\"stylesheet\" href=\"/static/css/colorize.css\">
  </head>

  <body>
    <span class=\"hidden\">Wooki - History of article: \"[0-9a-zA-Z]{20}\"</span>
    <div id=\"content\"><div id=\"content-area\"><h1>History of article <a href=\"/[0-9a-zA-Z]{20}\" class=\"internal\">[0-9a-zA-Z]{20}</a></h1>
    <a class=\"internal\" href=\"/site/feed/article.atom\\?title=[0-9a-zA-Z]{20}\">ATOM feed</a>
    <form method=\"post\" action=\"/site/history-with-undo\">
    <input type=\"hidden\" name=\"article\" value=\"[0-9a-zA-Z]{20}\" />
    <input type=\"submit\" value=\"Compare selected versions\" />
    <table id=\"pagehistory\"><tr><td>\\(<a class=\"internal\" href=\"/site/compare-revisions\\?article=[0-9a-zA-Z]{20}&old=[0-9]{10}&diff=[0-9]{10}\">prev</a>\\)</td><td><input type=\"radio\" name=\"old\" value=\"[0-9]{10}\" /></td><td><input type=\"radio\" name=\"diff\" value=\"[0-9]{10}\" /></td><td><a class=\"internal\" href=\"/site/view-revision\\?article=[0-9a-zA-Z]{20}&date=[0-9]{10}\">.+ [0-9]{4} .+ GMT</a> <a class=\"internal\" href=\"/site/account\\?name=4.5.6.7\">4.5.6.7</a> \\(<em>fact markup</em>\\) <input type=\"hidden\" name=\"undo-revision\" value=\"[0-9]{10}\" />
    \\(<input type=\"submit\" name=\"undo\" value=\"undo\" class=\"undo\" />\\)</td></tr><tr><td>\\(<a class=\"internal\" href=\"/site/compare-revisions\\?article=[0-9a-zA-Z]{20}&old=[0-9]{10}&diff=[0-9]{10}\">prev</a>\\)</td><td><input type=\"radio\" name=\"old\" value=\"[0-9]{10}\" /></td><td><input type=\"radio\" name=\"diff\" value=\"[0-9]{10}\" /></td><td><a class=\"internal\" href=\"/site/view-revision\\?article=[0-9a-zA-Z]{20}&date=[0-9]{10}\">.+ [0-9]{4} .+ GMT</a> <a class=\"internal\" href=\"/site/account\\?name=4.5.6.7\">4.5.6.7</a> \\(<em>a fact</em>\\) </td></tr><tr><td></td><td><input type=\"radio\" name=\"old\" value=\"[0-9]{10}\" /></td><td><input type=\"radio\" name=\"diff\" value=\"[0-9]{10}\" /></td><td><a class=\"internal\" href=\"/site/view-revision\\?article=[0-9a-zA-Z]{20}&date=[0-9]{10}\">.+ [0-9]{4} .+ GMT</a> <a class=\"internal\" href=\"/site/account\\?name=4.5.6.7\">4.5.6.7</a> \\(<em>created page</em>\\) </td></tr></table>
    <input type=\"submit\" value=\"Compare selected versions\" />
    </form></div>
  <div id=\"footer\" class=\"buttonbar\"><ul><li><a href=\"/[0-9a-zA-Z]{20}\">Current version</a></li>
      <li><a href=\"/site/history\\?article=[0-9a-zA-Z]{20}\">History</a></li>
      <li><a href=\"/site/backlinks\\?article=[0-9a-zA-Z]{20}\">Backlinks</a></li><li><a href=\"/site/edit-article\\?title=[0-9a-zA-Z]{20}&amp;from-revision=[0-9]{10}\">Edit</a></li><li><a href=\"/site/edit-article\\?create=t\">Create</a></li></ul></div>
  </div>
  <div id=\"header-buttons\" class=\"buttonbar\">
    <ul>
      <li><a href=\"/\">Home</a></li>
      <li><a href=\"/site/recent-changes\">Recent Changes</a></li>
      <li><a href=\"/Wooki\">About</a></li>
      <li><a href=\"/Text%20Formatting\">Text Formatting</a></li>
      <li><a href=\"/site/tools\">Tools</a></li>
    </ul>
    <div id=\"search\">
      <form action=\"/site/search\">
        <label for=\"search_query\" class=\"hidden\">Search Wooki</label>
        <input type=\"text\" name=\"query\" id=\"search_query\" value=\"\" />
        <input type=\"submit\" value=\"search\" />
      </form>
    </div>
  </div>
  <div id=\"pageheader\">
    <div id=\"header\">
      <span id=\"logo\">Wooki</span>
      <span id=\"slogan\">test wiki description</span>
      <div id=\"login\"><form method=\"post\" action=\"/site/login\">
                 <label for=\"login_name\" class=\"hidden\">Account name</label>
                 <input type=\"text\" name=\"name\" id=\"login_name\" class=\"login_input\" />
                 <label for= \"login_password\" class=\"hidden\">Password</label>
                 <input type=\"password\" name=\"password\" id=\"login_password\" class=\"login_input\" />
                 <input type=\"submit\" name=\"login\" value=\"login\" id=\"login_submit\" /><br />
                 <div id=\"register\"><a href=\"/site/register\">register</a></div>
                 <input type=\"submit\" name=\"reset-pw\" value=\"reset password\" id=\"reset_pw\" />
               </form>
      </div>
    </div>
  </div>
  </body></html>$"
       :single-line-mode t)
      (get-request 'cliki2::/site/history
                   "article" article-title)))))

(fiveam:test /site/not-latest1
  (let ((page (get-request 'cliki2::/site/not-latest
                           "article" "Some Article")))
    (fiveam:is
     (search
      "Can't undo this revision because it is not the latest.
  <a href=\"/site/history?article=Some%20Article\">Go back to history page</a>."
      page))))

(fiveam:test undo-latest-revision1
  (let* ((article-title (cliki2::make-random-string 20))
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title))))
    (mock 'hunchentoot:real-remote-addr (constantly "7.8.9.10")
      (cliki2::add-revision
       article "Potatoes are delicious" "created page")
      (cliki2::add-revision
       article "Potatoes are delicious /(fact)" "a fact")
      (cliki2::add-revision
       article "Potatoes are delicious *(fact)" "fact markup")

      (cliki2::undo-latest-revision
       (cliki2::find-article article-title))

      (let ((revisions (cliki2::revisions
                        (cliki2::find-article article-title))))
        (fiveam:is (= 4 (length revisions)))
        (fiveam:is
         (equal
          "undid last revision by 7.8.9.10"
          (cliki2::summary (car revisions))))))))

(fiveam:test undo-revision-stale-page
  (let* ((article-title (cliki2::make-random-string 20))
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title))))
    (mock 'hunchentoot:real-remote-addr (constantly "7.8.9.10")
      (cliki2::add-revision
       article "Potatoes are delicious" "created page")

      (let ((stale-revision
              (cliki2::add-revision
               article "Potatoes are delicious /(fact)" "first rev")))
        (cliki2::add-revision
         article "Potatoes are delicious *(fact)" "second rev")

        (fiveam:is
         (equal
          (concatenate 'string
                       "/site/not-latest?article="article-title)
          (cliki2::undo-revision
           article-title
           (write-to-string (cliki2::revision-date stale-revision)))))

        (fiveam:is (= 3 (length
                         (cliki2::revisions
                          (cliki2::find-article article-title)))))))))

(fiveam:test undo-revision1
  (let* ((article-title (cliki2::make-random-string 20))
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title))))
    (mock 'hunchentoot:real-remote-addr (constantly "7.8.9.10")
      (cliki2::add-revision
       article "Potatoes are delicious" "created page")
      (cliki2::add-revision
       article "Potatoes are delicious /(fact)" "a fact")
      (let ((latest-revision
              (cliki2::add-revision
               article "Potatoes are delicious *(fact)" "fact markup")))

        (mock 'cliki2::youre-banned? (constantly nil)
          (fiveam:is
           (equal
            (concatenate 'string "/"article-title)
            (cliki2::undo-revision
             article-title
             (write-to-string (cliki2::revision-date latest-revision))))))

        (let ((revisions (cliki2::revisions
                          (cliki2::find-article article-title))))
          (fiveam:is (= 4 (length revisions)))
          (fiveam:is
           (equal
            "undid last revision by 7.8.9.10"
            (cliki2::summary (car revisions)))))))))

(fiveam:test /site/history-with-undo1
  (ensure-redirected
      (post-request 'cliki2::/site/history-with-undo
                    ()
                    "article" "abc"
                    "old" "123"
                    "diff" "456")
      "/site/compare-revisions?article=abc&old=123&diff=456"))

(fiveam:test /site/history-with-undo2
  (let* ((article-title (cliki2::make-random-string 20))
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title))))
    (mock 'hunchentoot:real-remote-addr (constantly "7.8.9.10")
      (cliki2::add-revision
       article "Potatoes are delicious" "created page")
      (cliki2::add-revision
       article "Potatoes are delicious /(fact)" "a fact")
      (let ((latest-revision
              (cliki2::add-revision
               article "Potatoes are delicious *(fact)" "fact markup")))

        (mock 'cliki2::youre-banned? (constantly nil)
          (ensure-redirected
          (post-request 'cliki2::/site/history-with-undo
                        ()
                        "article" article-title
                        "old" "123"
                        "diff" "456"
                        "undo" "t"
                        "undo-revision"
                        (write-to-string
                         (cliki2::revision-date latest-revision)))
              (concatenate 'string "/"article-title)))

        (let ((revisions (cliki2::revisions
                          (cliki2::find-article article-title))))
          (fiveam:is (= 4 (length revisions)))
          (fiveam:is
           (equal
            "undid last revision by 7.8.9.10"
            (cliki2::summary (car revisions)))))))))

(fiveam:test /site/undo1
  (let* ((article-title (cliki2::make-random-string 20))
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title))))
    (mock 'hunchentoot:real-remote-addr (constantly "7.8.9.10")
      (cliki2::add-revision
       article "Potatoes are delicious" "created page")
      (cliki2::add-revision
       article "Potatoes are delicious /(fact)" "a fact")
      (let ((latest-revision
              (cliki2::add-revision
               article "Potatoes are delicious *(fact)" "fact markup")))

        (mock 'cliki2::youre-banned? (constantly nil)
          (ensure-redirected
          (post-request 'cliki2::/site/undo
                        ()
                        "article" article-title
                        "undo-revision"
                        (write-to-string
                         (cliki2::revision-date latest-revision)))
              (concatenate 'string "/"article-title)))

        (let ((revisions (cliki2::revisions
                          (cliki2::find-article article-title))))
          (fiveam:is (= 4 (length revisions)))
          (fiveam:is
           (equal
            "undid last revision by 7.8.9.10"
            (cliki2::summary (car revisions)))))))))
