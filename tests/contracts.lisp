;;;; Copyright 2023 Vladimir Sedach <vsedach@common-lisp.net>

;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public
;;;; License along with this program. If not, see
;;;; <http://www.gnu.org/licenses/>.

(in-package #:cliki2.tests)

(defparameter *contracts* (list))

(defun enable-contracts ()
  (dolist (c *contracts*)
    (handler-bind ((warning 'muffle-warning))
      (cl-advice:add-advice :around (car c) (cdr c)))))

(defun disable-contracts ()
  (dolist (c *contracts*)
    (cl-advice:make-unadvisable (car c))))

(defmacro define-contract (fname (&rest arglist) &body contract)
  `(pushnew (cons (quote ,fname)
                  (lambda ,arglist ,@contract))
            *contracts*))

(defun string-length (x)
  (assert (stringp x))
  (length x))

(defun strong-string= (x y)
  (and (stringp x)
       (stringp y)
       (string= x y)))

(defun strong-string-equal (x y)
  (and (stringp x)
       (stringp y)
       (string-equal x y)))

;;; accounts.lisp contracts

(defun account-name-invariant (account-name)
  (assert (< 2 (string-length account-name) 51)))

(defun account-password-invariant (password)
  (assert (< 8 (string-length password) 255)))

(define-contract cliki2::account-link (next account-name)
  (account-name-invariant account-name)
  (funcall next account-name))

(define-contract cliki2::password-digest (next password salt)
  (assert (stringp password))
  (assert (< 5 (string-length salt) 100))
  (let ((digest (funcall next password salt)))
    (assert (= 256 (string-length digest)))
    digest))

(define-contract cliki2::make-random-string (next length)
  (assert (integerp length))
  (assert (<= 0 length))
  (let ((rs (funcall next length)))
    (assert (= length (string-length rs)))
    rs))

(define-contract cliki2::maybe-show-form-error (next e ee msg)
  (when e (assert (stringp e)))
  (assert (member
           ee
           '(; /site/register
             "name" "nametaken" "email" "password" "captcha"
             ; /site/preferences
             "npw" "cpw" "opw" "pw"
             ; render-edit-article-common
             t
             )
           :test 'equal))
  (assert (< 10 (string-length msg)))
  (funcall next e ee msg))

(define-contract cliki2::password? (next x)
  (assert (stringp x))
  (let ((y (funcall next x)))
    (assert (typep y 'boolean))
    y))

(defun email-invariant (email)
  (assert (< 4 (string-length email) 255))
  ; TODO: regular expression validation
  )

(define-contract cliki2::email-address? (next x)
  (assert (stringp x))
  (let ((y (funcall next x)))
    (assert (typep y 'boolean))
    y))

(defun account-invariant (account)
  (assert (alexandria:proper-list-p account))
  (assert (= 5 (length account)))
  (account-name-invariant (cliki2::account-name account))
  (email-invariant (cliki2::account-email account))
  (assert (= 50 (string-length (cliki2::account-password-salt account))))
  (assert (= 256 (string-length
                  (cliki2::account-password-digest account))))
  (assert (typep (cliki2::account-admin account)
                 '(member nil :administrator :moderator))))

(define-contract cliki2::new-account (next name email pw)
  (account-name-invariant name)
  (email-invariant email)
  (account-password-invariant pw)
  (let ((acct (funcall next name email pw)))
    (account-invariant acct)
    (assert (strong-string= name (cliki2::account-name acct)))
    (assert (strong-string= email (cliki2::account-email acct)))
    (assert (cliki2::check-password pw acct))
    (assert (null (cliki2::account-admin acct)))
    acct))

(define-contract cliki2::reset-password (next account)
  (account-invariant account)
  (let ((old-account (cliki2::copy-account account))
        (x (funcall next account))
        (new-account (cliki2::find-account
                      (cliki2::account-name account))))
    (account-invariant new-account)
    (assert (not (strong-string=
                  (cliki2::account-password-salt old-account)
                  (cliki2::account-password-salt new-account))))
    (assert (not (strong-string=
                  (cliki2::account-password-digest old-account)
                  (cliki2::account-password-digest new-account))))
    x))

;;; article.lisp contracts

(define-contract cliki2::cut-whitespace (next str)
  (assert (stringp str))
  (let ((trimmed (funcall next str)))
    (assert (<= (string-length trimmed) (string-length str)))
    trimmed))

(defun article-title-invariant (article-title)
  (assert (< 2 (string-length article-title))))

(defun article-invariant (article)
  (assert (alexandria:proper-list-p article))
  (assert (= 2 (length article)))
  (article-title-invariant (cliki2::article-title article))
  (assert (alexandria:proper-list-p (cliki2::revisions article)))
  (when (cliki2::revisions article)
    (assert (apply '> (mapcar 'cliki2::revision-date
                              (cliki2::revisions article))))))

(define-contract cliki2::deleted? (next article)
  (article-invariant article)
  (let ((x (funcall next article)))
    (assert (typep x 'boolean))
    x))

(define-contract cliki2::article-link (next maybe-article-title)
  (assert (stringp maybe-article-title)) ; could be ""
  (let ((link (funcall next maybe-article-title)))
    (assert (stringp link))
    (assert (equal #\/ (elt link 0)))
    (assert (not (find #\Space link)))
    link))

(define-contract cliki2::latest-revision (next article)
  (article-invariant article)
  (funcall next article))

(define-contract cliki2::%print-article-link (next &rest args)
  (destructuring-bind (title class &optional (dt nil provided?))
      args
    (assert (stringp title))
    (assert (member class '("internal" "category") :test 'equal))
    (when provided? (assert (stringp dt))))
  (apply next args))

(define-contract cliki2::pprint-article-link (next title)
  (assert (stringp title))
  (funcall next title))

(define-contract cliki2::pprint-topic-link (next title)
  (assert (stringp title))
  (funcall next title))

(defun revision-date-invariant (date)
  (assert (integerp date))
  (assert (< (encode-universal-time 1 1 1 1 1 1998)
             date
             ;; TODO: fix time check in wiki.lisp
             (+ 100 (get-universal-time)))))

(defun revision-invariant (revision)
  (assert (alexandria:proper-list-p revision))
  (assert (= 6 (length revision)))
  (assert (stringp (cliki2::parent-title revision)))
  (revision-date-invariant (cliki2::revision-date revision))
  (assert (stringp (cliki2::summary revision)))
  (assert (< 3 (string-length (cliki2::author-name revision)) 255))
  (assert (< 2 (string-length (cliki2::author-ip revision)) 41)))

(define-contract cliki2::add-revision (next article content summary)
  (article-invariant article)
  (assert (stringp content))
  (assert (stringp summary))
  (let ((revision (funcall next article content summary)))
    (revision-invariant revision)
    revision))

(define-contract cliki2::edit-link (next revision anchor-text)
  (revision-invariant revision)
  (assert (strong-string-equal anchor-text "edit"))
  (funcall next revision anchor-text))

(define-contract cliki2::article-footer (next revision)
  (revision-invariant revision)
  (let ((footer (funcall next revision)))
    (assert (stringp footer))
    footer))

(define-contract cliki2::render-revision (next &rest args)
  (destructuring-bind (revision &optional (content nil provided?))
      args
    (revision-invariant revision)
    (when provided? (assert (stringp content))))
  (apply next args))

(define-contract cliki2::find-revision (next title datestring)
  (article-title-invariant title)
  (assert (stringp datestring))
  (revision-date-invariant (parse-integer datestring))
  (funcall next title datestring))

(define-contract cliki2::revision-link (next revision)
  (revision-invariant revision)
  (let ((link (funcall next revision)))
    (assert (= 0 (search "/site/view-revision?article=" link)))
    link))

(define-contract cliki2::pprint-revision-link (next revision)
  (revision-invariant revision)
  (funcall next revision))

(define-contract cliki2::render-edit-article-common (next &rest args)
  (destructuring-bind (maybe-title content summary
                       &key edit-title error)
      args
    (assert (stringp maybe-title))
    (assert (stringp content))
    (assert (stringp summary))
    (assert (typep edit-title 'boolean))
    (assert (typep error 'boolean)))
  (apply next args))

;;; authentication.lisp contracts

(defun session-invariant (session)
  (assert (cliki2::session-p session))
  (account-invariant (cliki2::session-account session))
  (assert (integerp (cliki2::session-expires-at session)))
  (assert (< (encode-universal-time 1 1 1 1 1 2012)
             (cliki2::session-expires-at session)
             (+ 1 (get-universal-time) (* 60 60 24 180))))
  (assert (= 256 (string-length
                  (cliki2::session-password-digest session)))))

(define-contract cliki2::logout (next)
  (wiki-invariant cliki2::*wiki*)
  (funcall next))

(define-contract cliki2::expire-old-sessions (next wiki)
  (wiki-invariant wiki)
  (funcall next wiki))

(define-contract cliki2::next-expiry-time (next)
  (let ((time (funcall next)))
    (assert (integerp time))
    (assert (< (get-universal-time)
               time
               (+ (get-universal-time) (* 60 60 24 365))))
    time))

(define-contract cliki2::login (next account)
  (account-invariant account)
  (wiki-invariant cliki2::*wiki*)
  (funcall next account))

(define-contract cliki2::account-auth (next)
  (wiki-invariant cliki2::*wiki*)
  (funcall next))

(defun captcha-invariants (captcha)
  (assert (alexandria:proper-list-p captcha))
  (assert (= 3 (length captcha)))
  (assert (integerp (eval captcha))))

(define-contract cliki2::make-captcha (next)
  (let ((captcha (funcall next)))
    (captcha-invariants captcha)
    captcha))

(define-contract cliki2::emit-captcha-inputs (next captcha class size)
  (captcha-invariants captcha)
  (assert (member class '("" "regin") :test 'equal))
  (assert (integerp size))
  (assert (< 10 size))
  (funcall next captcha class size))

(define-contract cliki2::check-captcha (next)
  (let ((result (funcall next)))
    (assert (typep result 'boolean))
    result))

;;; backlinks.lisp contracts

(define-contract cliki2::print-link-list (next links printer)
  (assert (alexandria:proper-list-p links))
  (assert (functionp printer))
  ; TODO: should be
  ; (assert (member printer '(pprint-topic-link pprint-article-link pprint-article-link)))
  (funcall next links printer)
  )

;;; diff.lisp contracts

(define-contract cliki2::choose-chunks (next chunks &rest kinds)
  (assert (alexandria:proper-list-p chunks))
  (assert (every (lambda (x) (typep x 'diff::chunk)) chunks))
  (assert (or (equalp kinds '(:delete :replace :create))
              (equalp kinds '(:create :insert :delete))))
  (apply next chunks kinds))

(define-contract cliki2::compare-strings (next a b)
  (assert (stringp a))
  (assert (stringp b))
  (multiple-value-bind (x y)
      (funcall next a b)
    (assert (stringp x))
    (assert (stringp y))
    (values x y)))

(define-contract cliki2::path-or-blank (next revision)
  (when revision
    (revision-invariant revision))
  (let ((file (funcall next revision)))
    (assert (probe-file file))
    file))

(define-contract cliki2::unified-diff-body (next old-revision new-revision)
  (when old-revision
   (revision-invariant old-revision))
  (revision-invariant new-revision)
  (funcall next old-revision new-revision))

(define-contract cliki2::revision-version-info-links (next revision)
  (revision-invariant revision)
  (funcall next revision))

(define-contract cliki2::render-unified-revision-diff
    (next old-revision new-revision)
  (when old-revision
    (revision-invariant old-revision))
  (revision-invariant new-revision)
  (funcall next old-revision new-revision))

(define-contract cliki2::render-diff-table
    (next old-revision new-revision undo-button?)
  (when old-revision
    (revision-invariant old-revision))
  (revision-invariant new-revision)
  (assert (typep undo-button? 'boolean))
  (funcall next old-revision new-revision undo-button?))

;;; dispatcher.lisp contracts

(define-contract cliki2::guess-article-name (next)
  (let ((maybe-article-name (funcall next)))
    (assert (stringp maybe-article-name))
    maybe-article-name))

(define-contract cliki2::show-deleted-article-page (next article)
  (article-invariant article)
  (funcall next article))

(define-contract cliki2::render-article (next &rest args)
  (destructuring-bind (article &optional (title nil p?))
      args
    (article-invariant article)
    (when p? (article-title-invariant title)))
  (let ((page-text (apply next args)))
    (assert (plausible-cliki-page? page-text))
    page-text))

;;; history.lisp contracts

(define-contract cliki2::output-undo-button (next revision)
  (revision-invariant revision)
  (funcall next revision))

(define-contract cliki2::output-compare-link
    (next old-revision new-revision text)
  (revision-invariant old-revision)
  (revision-invariant new-revision)
  (assert (member text '("diff" "prev") :test 'equal))
  (funcall next old-revision new-revision text))

(define-contract cliki2::undo-latest-revision (next article)
  (article-invariant article)
  (assert (<= 2 (length (cliki2::revisions article))))
  (funcall next article))

(define-contract cliki2::undo-revision
    (next article-title revision-date-string)
  (article-title-invariant article-title)
  (assert (stringp revision-date-string))
  (revision-date-invariant (parse-integer revision-date-string))
  (funcall next article-title revision-date-string))

;;; indexes.lisp contracts

(define-contract cliki2::canonicalize (next title)
  (assert (stringp title))
  (let ((x (funcall next title)))
    (assert (<= (string-length x) (string-length title)))
    x))

(define-contract cliki2::collect-links (next lt content)
  (assert (or (equal "\\*" lt)
              (equal "\\_" lt)))
  (assert (stringp content))
  (let ((x (funcall next lt content)))
    (assert (alexandria:proper-list-p x))
    x))

(define-contract cliki2::words (next content)
  (assert (stringp content))
  (let ((words (funcall next content)))
    (assert (alexandria:proper-list-p words))
    (assert (every 'stringp words))
    words))

(define-contract cliki2::search-articles (next phrase)
  (assert (stringp phrase))
  (let ((article-titles (funcall next phrase)))
    (assert (alexandria:proper-list-p article-titles))
    (assert (every 'stringp article-titles))
    article-titles))

(define-contract cliki2::paginate-article-summaries (next &rest args)
  (destructuring-bind (start article-titles &optional next-page-uri)
      args
    (declare (ignorable next-page-uri))
    (assert (or (null start) (stringp start)))
    (when start
      (assert (<= 0 (parse-integer start))))
    (assert (alexandria:proper-list-p article-titles))
    (assert (every 'stringp article-titles)))
  (apply next args))

;;; markup.lisp contract

(define-contract cliki2::generate-html-from-markup (next markup)
  (assert (stringp markup))
  (funcall next markup))

(defun check-escaper (next markup start end)
  (assert (stringp markup))
  (assert (integerp start))
  (assert (integerp end))
  (assert (<= 0 start end (string-length markup)))
  (let ((result (funcall next markup start end)))
    (assert (stringp result))
    result))

(define-contract cliki2::parse-markup-fragment (next markup start end)
  (check-escaper next markup start end))

(define-contract cliki2::escape-pre-block (next markup start end)
  (check-escaper next markup start end))

(define-contract cliki2::escape-parens-in-href-links
    (next markup start end)
  (check-escaper next markup start end))

(define-contract cliki2::parse-cliki-markup (next markup)
  (assert (stringp markup))
  (let ((result (funcall next markup)))
    (assert (stringp result))
    result))

(define-contract cliki2::pprint-article-underscore-link (next title)
  (assert (stringp title))
  (funcall next title))

(define-contract cliki2::article-description (next article-title)
  (article-title-invariant article-title)
  (funcall next article-title))

(define-contract cliki2::pprint-article-summary-li
    (next article-title separator)
  (article-title-invariant article-title)
  (assert (member separator '("-" "<br />") :test 'equal))
  (funcall next article-title separator))

(define-contract cliki2::format-topic-list (next topic)
  (assert (stringp topic))
  (funcall next topic))

(define-contract cliki2::format-hyperspec-link (next symbol-name)
  (assert (stringp symbol-name))
  (funcall next symbol-name))

(define-contract cliki2::format-package-link (next link)
  (assert (stringp link))
  (funcall next link))

(define-contract cliki2::markup-code (next markup start end)
  (check-escaper next markup start end))

;;; recent-changes.lisp contracts

(define-contract cliki2::find-previous-revision (next revision)
  (revision-invariant revision)
  (funcall next revision))

(define-contract cliki2::%render-revision-summary (next revision)
  (revision-invariant revision)
  (funcall next revision))

(define-contract cliki2::render-revision-summary (next revision)
  (revision-invariant revision)
  (funcall next revision))

(define-contract cliki2::iso8601-time (next time)
  (assert (integerp time))
  (let ((time-string (funcall next time)))
    (assert (< 10 (string-length time-string)))
    ; TODO: check with regular expression
    time-string))

(define-contract cliki2::feed-doc
    (next feed-title feed-link updated-time entries-body)
  (assert (stringp feed-title))
  (assert (stringp feed-link))
  (revision-date-invariant updated-time)
  (assert (typep entries-body 'function))
  (let ((atom-output
          (funcall next feed-title feed-link updated-time entries-body)))
    (assert (stringp atom-output))
    ; TODO: check Atom schema
    atom-output))

(define-contract cliki2::feed-format-content (next revision)
  (revision-invariant revision)
  (let ((result (funcall next revision)))
    (assert (stringp result))
    result))

(define-contract cliki2::feed-present-revision (next revision)
  (revision-invariant revision)
  (funcall next revision))

;;; wiki.lisp contracts

(defun wiki-invariant (wiki)
  (assert (cliki2::wiki-p wiki))
  (assert (uiop:directory-pathname-p (cliki2::home-directory wiki)))
  (assert (uiop:absolute-pathname-p (cliki2::home-directory wiki)))
  (assert (uiop:directory-exists-p (cliki2::home-directory wiki)))
  (assert (< 2 (string-length (cliki2::wiki-name wiki))))
  (assert (< 2 (string-length (cliki2::description wiki))))
  (email-invariant (cliki2::password-reminder-email-address wiki)))

(define-contract cliki2::find-account (next account-name)
  (wiki-invariant cliki2::*wiki*)
  (assert (stringp account-name))
  (let ((maybe-account (funcall next account-name)))
    (when maybe-account
      (account-invariant maybe-account))
    maybe-account))

(define-contract cliki2::banned? (next account-name)
  (wiki-invariant cliki2::*wiki*)
  (assert (stringp account-name))
  (let ((result (funcall next account-name)))
    (assert (typep result 'boolean))
    result))

(define-contract cliki2::get-blacklist (next)
  (wiki-invariant cliki2::*wiki*)
  (let ((blacklist (funcall next)))
    (assert (alexandria:proper-list-p blacklist))
    (assert (every 'stringp blacklist))
    blacklist))

(define-contract cliki2::find-article (next title &key error)
  (wiki-invariant cliki2::*wiki*)
  (assert (stringp title))
  (assert (typep error 'boolean))
  (let ((article (funcall next title :error error)))
    (or (null article)
        (article-invariant article))
    article))

(define-contract cliki2::get-all-articles (next predicate)
  (wiki-invariant cliki2::*wiki*)
  (assert (functionp predicate))
  (let ((article-titles (funcall next predicate)))
    (assert (alexandria:proper-list-p article-titles))
    (mapc 'article-title-invariant article-titles)
    article-titles))

(define-contract cliki2::edits-by-author (next author-name)
  (wiki-invariant cliki2::*wiki*)
  (account-name-invariant author-name) ; TODO: could be IP address
  (let ((revisions-by-author (funcall next author-name)))
    (assert (alexandria:proper-list-p revisions-by-author))
    (map nil 'revision-invariant revisions-by-author)
    revisions-by-author))

(define-contract cliki2::cached-content (next article-title)
  (wiki-invariant cliki2::*wiki*)
  (article-title-invariant article-title)
  (let ((cached-content (funcall next article-title)))
    (when cached-content (assert (stringp cached-content)))
    cached-content))

(define-contract cliki2::get-recent-changes (next)
  (wiki-invariant cliki2::*wiki*)
  (let ((recent-revisions (funcall next)))
    (assert (alexandria:proper-list-p recent-revisions))
    (map nil 'revision-invariant recent-revisions)
    recent-revisions))

(define-contract cliki2::wiki-path (next relative-path)
  (wiki-invariant cliki2::*wiki*)
  (assert (stringp relative-path))
  (assert (uiop:relative-pathname-p relative-path))
  (assert (or (member relative-path
                      '("empty_file" "blacklist")
                      :test 'equal)
              (some (lambda (dir)
                      (eql 0 (search dir relative-path)))
                    '("accounts/" "articles/" "static/" "tmp/"))))
  (let ((x (funcall next relative-path)))
    (assert (uiop:absolute-pathname-p x))
    x))

(define-contract cliki2::file-path (next type id)
  (assert (member type '(cliki2::account cliki2::article)))
  (assert (< 2 (string-length id) 255))
  (let ((file-path (funcall next type id)))
    (assert (uiop:absolute-pathname-p file-path))
    file-path))

(define-contract cliki2::write-to-file (next &rest args)
  (destructuring-bind (to-file obj &optional tmpdir)
      args
    (declare (ignorable obj tmpdir))
    (assert (uiop:absolute-pathname-p to-file))
    (assert (uiop:file-pathname-p to-file))
    (let ((file-count-before (length (uiop:directory-files to-file)))
          (file-count-expected-increase (if (probe-file to-file) 0 1))
          (x (apply next args)))
      (assert (probe-file to-file))
      (assert (= (+ file-count-before file-count-expected-increase)
                 (length (uiop:directory-files to-file))))
      x)))

(define-contract cliki2::wiki-new (next type data)
  (wiki-invariant cliki2::*wiki*)
  (assert (member type '(cliki2::article cliki2::account)))
  (let ((x (funcall next type data)))
    (funcall (ecase type
               (cliki2::article 'article-invariant)
               (cliki2::account 'account-invariant))
             x)
    x))

(define-contract cliki2::update-blacklist (next account-name banned?)
  (wiki-invariant cliki2::*wiki*)
  (account-name-invariant account-name)
  (assert (typep banned? 'boolean))
  (funcall next account-name banned?))

(define-contract cliki2::record-revision (next revision content)
  (wiki-invariant cliki2::*wiki*)
  (revision-invariant revision)
  (assert (stringp content))
  (let* ((revision-copy (cliki2::copy-revision revision))
         (article-title (cliki2::parent-title revision))
         (number-revisions (length (cliki2::revisions
                                    (cliki2::find-article article-title))))
         (result (funcall next revision content)))
    (declare (ignorable revision-copy))
    ;; TODO: restore after fixing record-revision time check
    ;; (assert (equalp revision-copy result))
    (assert (= (1+ number-revisions)
               (length (cliki2::revisions
                        (cliki2::find-article article-title)))))
    (assert (strong-string= content
                            (alexandria:read-file-into-string
                             (cliki2::revision-path revision))))
    result))

(define-contract cliki2::revision-path (next revision)
  (revision-invariant revision)
  (let ((revision-file (funcall next revision)))
    (assert (uiop:absolute-pathname-p revision-file))
    (assert (uiop:file-pathname-p revision-file))
    (assert (cl-ppcre:scan "articles/.+/revisions/[0-9]+"
                           (namestring revision-file)))
    revision-file))

(define-contract cliki2::revision-content (next revision)
  (revision-invariant revision)
  (funcall next revision))

(define-contract cliki2::save-revision-content (next revision content)
  (revision-invariant revision)
  (assert (stringp content))
  (funcall next revision content))

(define-contract cliki2::articles-by-search-word (next word)
  (wiki-invariant cliki2::*wiki*)
  (assert (stringp word))
  (let ((found-articles (funcall next word)))
    (assert (alexandria:proper-list-p found-articles))
    (map nil 'article-title-invariant found-articles)
    found-articles))

(define-contract cliki2::articles-by-topic (next topic)
  (wiki-invariant cliki2::*wiki*)
  (assert (stringp topic))
  (let ((found-articles (funcall next topic)))
    (assert (alexandria:proper-list-p found-articles))
    (map nil 'article-title-invariant found-articles)
    found-articles))

(define-contract cliki2::article-backlinks (next article-title)
  (wiki-invariant cliki2::*wiki*)
  (article-title-invariant article-title)
  (let ((found-articles (funcall next article-title)))
    (assert (alexandria:proper-list-p found-articles))
    (map nil 'article-title-invariant found-articles)
    found-articles))

(define-contract cliki2::all-topics (next)
  (wiki-invariant cliki2::*wiki*)
  (let ((previous-index-size (hash-table-count
                              (cliki2::topic-index cliki2::*wiki*)))
        (topic-list (funcall next)))
    ; all-topics may do cleanup for empty topics
    (assert (<= (hash-table-count (cliki2::topic-index cliki2::*wiki*))
                previous-index-size))
    (assert (alexandria:proper-list-p topic-list))
    (assert (every 'stringp topic-list))
    topic-list))

(define-contract cliki2::reindex-article
    (next title new-content old-content)
  (wiki-invariant cliki2::*wiki*)
  (article-title-invariant title)
  (assert (stringp new-content))
  (assert (stringp old-content))
  (funcall next title new-content old-content))

(define-contract cliki2::init-recent-changes (next)
  (wiki-invariant cliki2::*wiki*)
  (funcall next))

(define-contract cliki2::read-file (next &rest args)
  (let ((file (car args)))
    (assert (uiop:absolute-pathname-p file))
    (assert (uiop:file-pathname-p file))
    (assert (probe-file file)))
  (apply next args))

(define-contract cliki2::load-wiki-article (next article-directory)
  (wiki-invariant cliki2::*wiki*)
  (assert (uiop:absolute-pathname-p article-directory))
  (assert (uiop:directory-pathname-p article-directory))
  (assert (uiop:directory-exists-p article-directory))
  (funcall next article-directory)
  ;; load-wiki-article should not return anything
  (values)
  )

(define-contract cliki2::load-wiki (next wiki)
  (wiki-invariant wiki)
  (funcall next wiki)
  ;; load-wiki should not return anything
  (values))

(define-contract cliki2::make-wiki (next name desc dir email)
  (assert (< 2 (string-length name)))
  (assert (stringp desc))
  (assert (uiop:absolute-pathname-p dir))
  (assert (uiop:directory-pathname-p dir))
  (assert (uiop:directory-exists-p dir))
  (email-invariant email)
  (let ((wiki (funcall next name desc dir email)))
    (wiki-invariant wiki)
    wiki))
