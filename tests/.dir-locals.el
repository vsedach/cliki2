;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil
  (indent-tabs-mode)
  (fill-column . 69))
 (lisp-mode
  (eval put 'markup-test 'common-lisp-indent-function
        '(4 &body))
  (eval put 'test 'common-lisp-indent-function
        '(4 &body))
  (eval put 'test-str 'common-lisp-indent-function
        '(4 &body))
  ))
