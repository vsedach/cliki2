;;;; Copyright 2023 Vladimir Sedach <vsedach@common-lisp.net>

;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public
;;;; License along with this program. If not, see
;;;; <http://www.gnu.org/licenses/>.

(in-package #:cliki2.tests)

(fiveam:def-suite account-tests :in cliki2-tests)
(fiveam:in-suite account-tests)

;;;; Subroutine tests

(test-str account-link1
  "<a class=\"internal\" href=\"/site/account?name=user1\">user1</a>"
  (cliki2::account-link "user1"))

(test-str password-digest1
  "355d919196e56e6606cd7f92b75ad07241bd3ee0580a63b52c68093b6a16b785ab7c81c3355b124146448413a372db8e8faf69239ca0a090e064e44c0ceddc05967588d14d98120ec228d5392354b6f1addd3b78964852f5ddfa73d52cee0f1e3c5f9baea719f03d8964f21af3ad5a45894bff34cf30e08b16fc0e68e850601d"
  (cliki2::password-digest "password1" "abc123"))

(test-str make-random-string1
  ""
  (cliki2::make-random-string 0))

(fiveam:test make-random-string2
  (dotimes (i 100)
    (let ((rs (cliki2::make-random-string i)))
      (fiveam:is (= i (length rs)))
      (fiveam:is (every (lambda (x)
                          (find x "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"))
                        rs))
      (when (> i 20)
        (fiveam:is (not (strong-string=
                         rs (cliki2::make-random-string i))))))))

(fiveam:test maybe-show-form-error1
  (fiveam:is
   (equal
    ""
    (html-output
      (cliki2::maybe-show-form-error
       "name" "nametaken" "Name required")))))

(fiveam:test maybe-show-form-error2
  (fiveam:is
   (equal
    "<div class=\"error-info\">Name required</div>"
    (html-output
      (cliki2::maybe-show-form-error "name" "name" "Name required")))))

(test-pred password?0 (not (cliki2::password? "")))

(test-pred password?1 (not (cliki2::password? "1234")))

(test-pred password?2 (cliki2::password? "12345678"))

(test-pred password?3 (cliki2::password? "123456789"))

(test-pred password?4 (cliki2::password? "123456789abcdf"))

(test-pred email-address?0 (not (cliki2::email-address? "")))

(test-pred email-address?1 (cliki2::email-address? "abc@xyz.com"))

(fiveam:test new-account0
  (let* ((new-name (cliki2::make-random-string 10))
         (new-acct (cliki2::new-account
                    new-name "person2@aol.com" "password1")))
    (fiveam:is (strong-string= new-name (cliki2::account-name new-acct)))
    (fiveam:is (equal "person2@aol.com"
                      (cliki2::account-email new-acct)))
    (fiveam:is-true (cliki2::check-password "password1" new-acct))
    (fiveam:is-false (cliki2::account-admin new-acct))
    (fiveam:is (equal new-acct (cliki2::find-account new-name)))
    (fiveam:is (equal new-acct
                      (cliki2::read-file
                       (cliki2::file-path 'cliki2::account new-name))))))

(fiveam:test reset-password0
  (let* ((account-name (cliki2::make-random-string 15))
         (account (cliki2::new-account account-name "person@aol.com" "password2")))
   (mock 'cl-smtp:send-email
       (lambda (next-func host from to subject msg)
         (declare (ignore next-func))
         (fiveam:is (equal "localhost" host))
         (fiveam:is (equal "wiki@domain.com" from))
         (fiveam:is (equal "person@aol.com" to))
         (fiveam:is (equal "Your new Wooki wiki password" subject))
         (fiveam:is
          (= 0
             (cl-ppcre:scan
              "^Someone \\(hopefully you\\) requested a password reset for a lost password on the Wooki wiki.
Your new password is: [0-9a-zA-Z]{14}$"
              msg))))
     (cliki2::reset-password account))))

(test-pred check-password0 (not (cliki2::check-password "" nil)))

(fiveam:test check-password1
  (let ((new-acct (cliki2::new-account
                   (cliki2::make-random-string 20)
                   "person2@aol.com"
                   "password1")))
    (fiveam:is-true (cliki2::check-password "password1" new-acct))
    (fiveam:is-false (cliki2::check-password "passwordX" new-acct))
    (fiveam:is-false (cliki2::check-password "password" new-acct))
    (fiveam:is-false (cliki2::check-password "" new-acct))))

;;;; Handler tests

(fiveam:def-suite account-handler-tests :in cliki2-tests)
(fiveam:in-suite account-handler-tests)

;;; /site/register

(fiveam:test /site/register0
  (let ((page (get-request 'cliki2::/site/register)))

    (fiveam:is
     (search
      "<h3>Create account</h3>
  <form id=\"registration\" class=\"prefs\" method=\"post\" action=\"/site/do-register\">
  <dl><dt><label for=\"name\">Name:</label></dt>
    <dd><input class=\"regin\" name=\"name\" size=\"30\" value=\"\" /></dd><dt><label for=\"email\">Email:</label></dt>
    <dd><input class=\"regin\" name=\"email\" size=\"30\" value=\"\" /></dd><dt><label for=\"password\">Password:</label></dt>
       <dd><input class=\"regin\" name=\"password\" type=\"password\" size=\"30\" /></dd><dt><label for=\"captcha\">"
      page))

    (fiveam:is
     (search "<input type=\"hidden\" name=\"captcha" page))

    (fiveam:is
     (search "</dd><dt /><dd><input type=\"submit\" value=\"Create account\" /></dd>
  </dl>
  </form>
</div>" page)
     )))

(fiveam:test /site/register-redirect-banned
  (ensure-redirected
      (mock 'cliki2::banned? (constantly t)
        (get-request 'cliki2::/site/register))
      "/"))

(fiveam:test /site/register-redirect-logged-in
  (ensure-redirected
      (let ((cliki2::*account* (list "A.B. Contributor")))
        (get-request 'cliki2::/site/register))
      "/"))

;;; /site/do-register

(fiveam:test /site/do-register-banned
  (ensure-redirected
      (mock 'cliki2::banned? (constantly t)
        (post-request 'cliki2::/site/do-register))
      "/"))

(fiveam:test /site/do-register-noname
  (ensure-redirected
      (post-request
       'cliki2::/site/do-register
       ()
       "name" ""
       "email" "abc@xyz.com"
       "password" "secret1")
      "/site/register?name=&email=abc%40xyz.com&error=name"))

(fiveam:test /site/do-register-name-taken
  (let ((new-user-name (cliki2::make-random-string 20)))
    (cliki2::new-account new-user-name "nametaken@aol.com" "password1")
    (ensure-redirected
        (post-request
         'cliki2::/site/do-register
         ()
         "name" new-user-name
         "email" "abc@xyz.com"
         "password" "secret1")
        (concatenate 'string
                     "/site/register?name="
                     new-user-name
                     "&email=abc%40xyz.com&error=nametaken"))))

(fiveam:test /site/do-register-bad-email
  (ensure-redirected
      (post-request
       'cliki2::/site/do-register
       ()
       "name" "Contributor"
       "email" ""
       "password" "secret1")
      "/site/register?name=Contributor&email=&error=email"))

(fiveam:test /site/do-register-bad-password
  (ensure-redirected
      (post-request
       'cliki2::/site/do-register
       ()
       "name" "Bob"
       "email" "zzz@aol.net"
       "password" "short")
      "/site/register?name=Bob&email=zzz%40aol.net&error=password"))

(fiveam:test /site/do-register-bad-captcha
  (ensure-redirected
      (post-request
       'cliki2::/site/do-register
       ()
       "name" "Bob"
       "email" "zzz@aol.net"
       "password" "longenough"
       "captcha-x" "12"
       "captcha-y" "4"
       "captcha-op" "floor"
       "captcha-answer" "1")
      "/site/register?name=Bob&email=zzz%40aol.net&error=captcha"))

(fiveam:test /site/do-register-success
  (let ((hunchentoot:*reply* (make-instance 'hunchentoot:reply)))
    (ensure-redirected
        (post-request
         'cliki2::/site/do-register
         ()
         "name" "Bob"
         "email" "zzz@aol.net"
         "password" "longenough"
         "captcha-x" "12"
         "captcha-y" "4"
         "captcha-op" "floor"
         "captcha-answer" "3")
        "/")
    (fiveam:is (= 60 (string-length
                      (hunchentoot:cookie-value
                       (hunchentoot:cookie-out "cliki2auth")))))))

;;; /site/reset-ok

(fiveam:test /site/reset-ok0
  (let ((page (get-request 'cliki2::/site/reset-ok)))
    (fiveam:is
     (search "Password reset successfully. Check your inbox."
             page))))

;;; /site-login

(fiveam:test /site/login-already-logged-in
  (let ((cliki2::*account* (list "Al Ready")))
    (ensure-redirected
        (post-request
         'cliki2::/site/login
         '(:headers-in ((:referer . "/somewhere")))
         "name" ""              ; TODO: fix /site/login to handle nil
         )
        "/somewhere")))

(fiveam:test /site/login0
  (let ((hunchentoot:*reply* (make-instance 'hunchentoot:reply))
        (acct (cliki2::make-random-string 20)))
    (cliki2::new-account acct "bob@bob.bob" "password5")

    (ensure-redirected
        (post-request
         'cliki2::/site/login
         '(:headers-in ((:referer . "/some-article")))
         "name" acct
         "password" "password5")
        "/some-article")

    (fiveam:is (= 60 (string-length
                      (hunchentoot:cookie-value
                       (hunchentoot:cookie-out "cliki2auth")))))))

;;; /site/invalid-login

(fiveam:test /site/invalid-login0
  (let ((page (get-request 'cliki2::/site/invalid-login)))
    (fiveam:is (search "Account name and/or password is incorrect"
                       page))))

;;; /site/cantfind

(fiveam:test /site/cantfind0
  (let ((page (get-request 'cliki2::/site/cantfind
                           "name" "Bobby Spammer")))
    (fiveam:is
     (search "Account with name 'Bobby Spammer' doesn't exist"
             page))))

;;; /site/logout

(fiveam:test /site/logout0
  (let ((hunchentoot:*reply* (make-instance 'hunchentoot:reply)))
    (ensure-redirected
        (get-request 'cliki2::/site/logout)
        "/")
    (fiveam:is (equal ""
                      (hunchentoot:cookie-value
                       (hunchentoot:cookie-out "cliki2auth"))))))

;;; /site/account

(fiveam:test /site/account-cantfind
  (ensure-redirected
      (get-request 'cliki2::/site/account "name" "DoesNotExist")
      "/site/cantfind?name=DoesNotExist"))

(fiveam:test /site/account0
  (let ((account-name (cliki2::make-random-string 20)))
    (cliki2::new-account account-name "bob@aol.com" "password1")
    (let ((page (get-request 'cliki2::/site/account
                             "name" account-name)))
      (fiveam:is (search "account info page</h1>" page))
      (fiveam:is (search "<br />User page: " page))
      (fiveam:is (search "<br />Edits by " page))
      )))

;;; /site/preferences-ok

(fiveam:test /site/preferences-ok0
  (let ((page (get-request 'cliki2::/site/preferences-ok
                           "what" "Password")))
    (fiveam:is
     (search "Password updated successfully"
             page))))

;;; /site/change-email

(fiveam:test /site/change-email-not-logged-in
  (ensure-redirected
      (post-request 'cliki2::/site/change-email)
      "/"))

(fiveam:test /site/change-email-bad-new-address
  (let ((cliki2::*account* (list "Bob")))
    (ensure-redirected
        (post-request 'cliki2::/site/change-email
                     ()
                     "email" "")
        "/site/preferences?email=&error=email")))

(fiveam:test /site/change-email-password-does-not-match
  (let ((cliki2::*account* (list "Bad PW")))
    (ensure-redirected
       (post-request 'cliki2::/site/change-email
                    ()
                    "email" "abc@hotmail.com")
       "/site/preferences?email=abc%40hotmail.com&error=pw")))

(fiveam:test /site/change-email0
  (let ((cliki2::*account*
          (cliki2::new-account
           "Obo" "person@aol.com" "password2")))
    (ensure-redirected
        (post-request 'cliki2::/site/change-email
                      ()
                      "email" "bot@hotmail.com"
                      "password" "password2")
        "/site/preferences-ok?what=Email")
    (fiveam:is
     (equal
      "bot@hotmail.com"
      (cliki2::account-email (cliki2::find-account "Obo"))))))

;;; /site/change-password

(fiveam:test /site/change-password-not-logged-in
  (ensure-redirected
      (post-request 'cliki2::/site/change-password)
      "/"))

(fiveam:test /site/change-password-bad-new-password
  (let ((cliki2::*account* (list "Bob")))
    (ensure-redirected
        (post-request 'cliki2::/site/change-password
                     ()
                     "new-password" "short")
        "/site/preferences?error=npw")))

(fiveam:test /site/change-password-confirmation-does-not-match
  (let ((cliki2::*account* (list "Bad PW")))
    (ensure-redirected
       (post-request 'cliki2::/site/change-password
                    ()
                    "new-password" "password1"
                    "confirm-password" "password2")
       "/site/preferences?error=cpw")))

(fiveam:test /site/change-password-password-does-not-match
  (let ((cliki2::*account* (list "Bad PW")))
    (ensure-redirected
        (post-request 'cliki2::/site/change-password
                      ()
                      "new-password" "password1"
                      "confirm-password" "password1")
        "/site/preferences?error=opw")))

(fiveam:test /site/change-password0
  (let ((cliki2::*account*
          (cliki2::new-account
           "Obo" "person@aol.com" "old-password")))
    (ensure-redirected
        (post-request 'cliki2::/site/change-password
                      ()
                      "new-password" "password1"
                      "confirm-password" "password1"
                      "password" "old-password")
        "/site/preferences-ok?what=Password")
    (fiveam:is-true
     (cliki2::check-password
      "password1" (cliki2::find-account "Obo")))))

;;; /site/preferences

(fiveam:test /site/preferences-not-logged-in
  (ensure-redirected
      (post-request 'cliki2::/site/preferences)
      "/"))

(fiveam:test /site/preferences0
  (let ((cliki2::*account* (list "Al Ready")))
    (let ((page (get-request 'cliki2::/site/preferences)))

      (fiveam:is
       (search
        "<h3>Change account preferences</h3>
        <form id=\"changepassword\" class=\"prefs\" method=\"post\"
              action=\"/site/change-password\">
	<fieldset class=\"prefs\">
	<legend>Password</legend>
        <dl>"
        page))

      (fiveam:is
       (search
        "<dt><label for=\"new-password\">New password:</label></dt>
          <dd><input class=\"regin\" type=\"password\" name=\"new-password\" title=\"new password\" /></dd>"
        page))

      (fiveam:is
       (search
        "<dt><label for=\"confirm-password\">Confirm password:</label></dt>
          <dd><input class=\"regin\" type=\"password\" name=\"confirm-password\" title=\"confirm password\" /></dd>"
        page))

      (fiveam:is
       (search
        "<dt><label for=\"password\">Old password:</label></dt>
          <dd><input class=\"regin\" type=\"password\" name=\"password\" /></dd>
          <dt /><dd><input type=\"submit\" value=\"change password\" /></dd>
        </dl>
	</fieldset>
	</form>"
        page))

      (fiveam:is
       (search
        "<form id=\"changemail\" class=\"prefs\" method=\"post\"
              action=\"/site/change-email\">
	<fieldset class=\"prefs\">
	<legend>Email</legend>
        <dl>"
        page))

      (fiveam:is
       (search
        "<dt><label for=\"email\">New email:</label></dt>
          <dd><input class=\"regin\" type=\"text\" name=\"email\" title=\"new email\"
                     value=\""
        page))

      (fiveam:is
       (search
        "<dt><label for=\"password\">Enter password:</label></dt>
          <dd><input class=\"regin\" type=\"password\" name=\"password\" /></dd>
          <dt /><dd><input type=\"submit\" value=\"change email\" /></dd>
        </dl>
	</fieldset>
	</form>"
        page)))))

;;; /site/make-moderator

(fiveam:test /site/make-moderator-youre-banned
  (ensure-redirected
      (mock 'cliki2::banned? (constantly t)
        (post-request 'cliki2::/site/make-moderator
                      () "name" "Faker"))
      "/"))

(fiveam:test /site/make-moderator-youre-not-a-mod
  (let ((cliki2::*account* (list "XYZ" "" "" "" nil)))
    (ensure-redirected
        (post-request 'cliki2::/site/make-moderator
                      '(:headers-in ((:referer . "/somewhere")))
                      "name" "Faker")
        "/somewhere")))

(fiveam:test /site/make-moderator-no-such-account
  (let ((cliki2::*account* (list "XYZ" "" "" "" :moderator)))
    (ensure-redirected
        (post-request 'cliki2::/site/make-moderator
                      () "name" "DoesNotExist")
        "/site/cantfind?name=DoesNotExist")))

(fiveam:test /site/make-moderator0
  (let* ((cliki2::*account* (list "XYZ" "" "" "" :moderator))
         (modname (cliki2::make-random-string 20))
         (newmod (cliki2::new-account modname "mod@mod.com" "password1")))
    (fiveam:is-false (cliki2::account-admin newmod))
    (ensure-redirected
        (post-request 'cliki2::/site/make-moderator
                      '(:headers-in ((:referer . "/somepage")))
                      "name" modname)
        "/somepage")
    (fiveam:is (eql :moderator (cliki2::account-admin
                                (cliki2::find-account modname))))))

;;; TODO: fix handler
;; (fiveam:test /site/make-moderator-already-administrator
;;   (let* ((cliki2::*account* (list "XYZ" "" "" "" :moderator))
;;          (modname (cliki2::make-random-string 20))
;;          (admin (cliki2::new-account modname "mod@mod.com" "password1")))
;;     (cliki2::update-account admin cliki2::account-admin :administrator)
;;     (ensure-redirected
;;         (post-request 'cliki2::/site/make-moderator
;;                       '(:headers-in ((:referer . "/somepage")))
;;                       "name" modname)
;;         "/somepage")
;;     (fiveam:is (eql :administrator (cliki2::account-admin
;;                                     (cliki2::find-account modname))))))

;;; /site/ban

(fiveam:test /site/ban-youre-banned
  (ensure-redirected
      (mock 'cliki2::banned? (constantly t)
        (post-request 'cliki2::/site/ban
                      () "name" "Faker"))
      "/"))

(fiveam:test /site/ban-youre-not-a-mod
  (let ((cliki2::*account* (list "XYZ" "" "" "" nil)))
    (ensure-redirected
        (post-request 'cliki2::/site/ban
                      '(:headers-in ((:referer . "/somewhere")))
                      "name" "Faker")
        "/somewhere")))

(fiveam:test /site/ban-spammer-account
  (let* ((cliki2::*account* (list "XYZ" "" "" "" :moderator))
         (spammer (cliki2::make-random-string 20)))
    (cliki2::new-account spammer "spam@spam.spam" "spamspamspam")
    (fiveam:is-false (cliki2::banned? spammer))
    (ensure-redirected
        (post-request 'cliki2::/site/ban
                      '(:headers-in ((:referer . "/somepage")))
                      "name" spammer)
        "/somepage")
    (fiveam:is (cliki2::banned? spammer))))

(fiveam:test /site/ban-spammer-ipv4
  (let* ((cliki2::*account* (list "XYZ" "" "" "" :moderator))
         (spam-ip "1.2.3.4"))
    (fiveam:is-false (cliki2::banned? spam-ip))
    (ensure-redirected
        (post-request 'cliki2::/site/ban
                      '(:headers-in ((:referer . "/somepage")))
                      "name" spam-ip)
        "/somepage")
    (fiveam:is (cliki2::banned? spam-ip))))

(fiveam:test /site/ban-spammer-ipv6
  (let* ((cliki2::*account* (list "XYZ" "" "" "" :moderator))
         (spam-ip "abc:123:fe4::1"))
    (fiveam:is-false (cliki2::banned? spam-ip))
    (ensure-redirected
        (post-request 'cliki2::/site/ban
                      '(:headers-in ((:referer . "/somepage")))
                      "name" spam-ip)
        "/somepage")
    (fiveam:is (cliki2::banned? spam-ip))))

;;; TODO: fix ban logic
;; (fiveam:test /site/ban-cant-ban-localhost-ipv4
;;   (let* ((cliki2::*account* (list "XYZ" "" "" "" :moderator))
;;          (spam-ip "127.0.0.1"))
;;     (ensure-redirected
;;         (post-request 'cliki2::/site/ban
;;                       '(:headers-in ((:referer . "/somepage")))
;;                       "name" spam-ip)
;;         "/somepage")
;;     (fiveam:is-false (cliki2::banned? spam-ip))))

;; (fiveam:test /site/ban-cant-ban-localhost-ipv6
;;   (let* ((cliki2::*account* (list "XYZ" "" "" "" :moderator))
;;          (spam-ip "::1"))
;;     (ensure-redirected
;;         (post-request 'cliki2::/site/ban
;;                       '(:headers-in ((:referer . "/somepage")))
;;                       "name" spam-ip)
;;         "/somepage")
;;     (fiveam:is-false (cliki2::banned? spam-ip))))

;; (fiveam:test /site/ban-reject-random-stuff
;;   (let* ((cliki2::*account* (list "XYZ" "" "" "" :moderator))
;;          (spam (cliki2::make-random-string 2000)))
;;     (ensure-redirected
;;         (post-request 'cliki2::/site/ban
;;                       '(:headers-in ((:referer . "/somepage")))
;;                       "name" spam)
;;         "/somepage")
;;     (fiveam:is-false (cliki2::banned? spam))))

;; (fiveam:test /site/ban-cant-ban-yourself
;;   (let* ((you (cliki2::make-random-string 20))
;;          (acct (cliki2::new-account you "mod1@mod.com" "password1")))
;;     (cliki2::update-account acct cliki2::account-admin :moderator)
;;     (fiveam:is-false (cliki2::banned? you))
;;     (let ((cliki2::*account* (cliki2::find-account you)))
;;       (ensure-redirected
;;           (post-request 'cliki2::/site/ban
;;                         '(:headers-in ((:referer . "/fail")))
;;                         "name" you)
;;           "/fail"))
;;     (fiveam:is-false (cliki2::banned? you))))

;;; TODO: write test
;; (fiveam:test /site/ban-cant-ban-own-ip)

  (fiveam:test /site/ban-mod-cant-ban-administrator
    (let* ((cliki2::*account* (list "XYZ" "" "" "" :moderator))
           (admin (cliki2::make-random-string 20))
           (adminacct (cliki2::new-account admin "mod@mod.com" "password1")))
      (cliki2::update-account adminacct cliki2::account-admin :administrator)
      (ensure-redirected
          (post-request 'cliki2::/site/ban
                        '(:headers-in ((:referer . "/fail")))
                        "name" admin)
          "/fail")
      (fiveam:is-false (cliki2::banned? admin))))

(fiveam:test /site/ban-admin-cant-ban-administrator
  (let* ((cliki2::*account* (list "XYZ" "" "" "" :administrator))
         (admin (cliki2::make-random-string 20))
         (adminacct (cliki2::new-account admin "mod@mod.com" "password1")))
    (cliki2::update-account adminacct cliki2::account-admin :administrator)
    (ensure-redirected
        (post-request 'cliki2::/site/ban
                      '(:headers-in ((:referer . "/fail")))
                      "name" admin)
        "/fail")
    (fiveam:is-false (cliki2::banned? admin))))

;;; /site/unban

(fiveam:test /site/unban-youre-banned
  (ensure-redirected
      (mock 'cliki2::banned? (constantly t)
        (post-request 'cliki2::/site/unban
                      () "name" "Faker"))
      "/"))

(fiveam:test /site/unban-youre-not-a-mod
  (let ((cliki2::*account* (list "XYZ" "" "" "" nil)))
    (ensure-redirected
        (post-request 'cliki2::/site/unban
                      '(:headers-in ((:referer . "/somewhere")))
                      "name" "Faker")
        "/somewhere")))

(fiveam:test /site/unban0
  (let ((cliki2::*account* (list "XYZ" "" "" "" :moderator))
        (not-a-spammer (cliki2::make-random-string 20)))
    (cliki2::update-blacklist not-a-spammer t)
    (fiveam:is-true (cliki2::banned? not-a-spammer))
    (ensure-redirected
        (post-request 'cliki2::/site/unban
                      '(:headers-in ((:referer . "/whatever")))
                      "name" not-a-spammer)
        "/whatever")
    (fiveam:is-false (cliki2::banned? not-a-spammer))))
