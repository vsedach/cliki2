;;;; Copyright 2023 Vladimir Sedach <vsedach@common-lisp.net>

;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public
;;;; License along with this program. If not, see
;;;; <http://www.gnu.org/licenses/>.

(in-package #:cliki2.tests)

(fiveam:def-suite article-tests :in cliki2-tests)
(fiveam:in-suite article-tests)

;;; cut-whitespace

(test-str cut-whitespace1
  "abc 123"
  (cliki2::cut-whitespace "abc 123"))

(test-str cut-whitespace2
  "abc 123"
  (cliki2::cut-whitespace "
 abc
 123  "))

(fiveam:test cut-whitespace-random
  (flet ((random-whitespace ()
           (alexandria:random-elt #(#\Space #\Tab #\Newline #\Return))))
    (dotimes (i 20)
     (let ((words (map-into (make-list (random 10))
                            (lambda ()
                              (cliki2::make-random-string (random 5))))))

       (fiveam:is
        (format nil "~{~A~^ ~}" words)
        (cliki2::cut-whitespace
         (with-output-to-string (out)
           (map nil
                (lambda (w)
                  (princ (random-whitespace) out)
                  (princ w out))
                words)
           (princ (random-whitespace) out))))))))

;;; deleted?

(fiveam:test deleted1
  (let ((article-name (cliki2::make-random-string 20)))
    (setf (gethash (string-downcase article-name)
                   (cliki2::article-cache cliki2::*wiki*))
          "")
    (fiveam:is-true (cliki2::deleted? (list article-name ())))))

(fiveam:test deleted2
  (let ((article-name (cliki2::make-random-string 20)))
    (setf (gethash article-name (cliki2::article-cache cliki2::*wiki*))
          "something etc")
    (fiveam:is-false (cliki2::deleted? (list article-name ())))))

;;; article-link

(test-str article-link1
  "/common%20lisp"
  (cliki2::article-link "common lisp"))

;;; latest-revision

(fiveam:test latest-revision0
  (fiveam:is
   (null
    (cliki2::latest-revision (list "abc" (list))))))

(fiveam:test latest-revision1
  (fiveam:is
   (equalp
    '("revision mock" 1)
    (cliki2::latest-revision (list "abc" '(("revision mock" 1)))))))

(fiveam:test latest-revision2
  (fiveam:is
   (equalp
    '("revision mock" 2)
    (cliki2::latest-revision
     (list "abc" '(("revision mock" 2) ("foobar" 1)))))))

;;; pprint-article-link

(test-str pprint-article-link-does-not-exist
  "<a href=\"/DoesNotExist\" class=\"new\">DoesNotExist</a>"
  (html-output (cliki2::pprint-article-link "DoesNotExist")))

(fiveam:test pprint-article-link2
  (let ((article-title (cliki2::make-random-string 20)))
    (cliki2::wiki-new 'cliki2::article
                      (cliki2::make-article :article-title article-title))
    (fiveam:is
     (equal
      (format nil "<a href=\"/~A\" class=\"internal\">~A</a>"
              article-title article-title)
      (html-output (cliki2::pprint-article-link article-title))))))

(fiveam:test pprint-article-link-deleted
  (let* ((article-title (cliki2::make-random-string 20))
         (article (cliki2::wiki-new
                   'cliki2::article
                   (cliki2::make-article :article-title article-title))))
    (setf (gethash (string-downcase article-title)
                   (cliki2::article-cache cliki2::*wiki*))
          "")
    (fiveam:is-true (cliki2::deleted? article))
    (fiveam:is
     (equal
      (format nil "<a href=\"/~A\" class=\"new\">~A</a>"
              article-title article-title)
      (html-output (cliki2::pprint-article-link article-title))))))

;;; pprint-topic-link

(test-str pprint-topic-link-does-not-exist
  "<a href=\"/Does%20Not%20Exist\" class=\"new\">Does Not Exist</a>"
  (html-output (cliki2::pprint-topic-link "Does Not Exist")))

(fiveam:test pprint-topic-link2
  (let ((topic-title (cliki2::make-random-string 20)))
    (cliki2::wiki-new
     'cliki2::article
     (cliki2::make-article :article-title topic-title))
    (fiveam:is
     (equal
      (format nil "<a href=\"/~A\" class=\"category\">~A</a>"
              topic-title topic-title)
      (html-output (cliki2::pprint-topic-link topic-title))))))

(fiveam:test pprint-topic-link-deleted
  (let* ((topic-title (cliki2::make-random-string 20))
         (article (cliki2::wiki-new
                   'cliki2::article
                   (cliki2::make-article :article-title topic-title))))
    (setf (gethash (string-downcase topic-title)
                   (cliki2::article-cache cliki2::*wiki*))
          "")
    (fiveam:is-true (cliki2::deleted? article))
    (fiveam:is
     (equal
      (format nil "<a href=\"/~A\" class=\"new\">~A</a>"
              topic-title topic-title)
      (html-output (cliki2::pprint-topic-link topic-title))))))

;;; add-revision

(fiveam:test add-revision1
  (let* ((article-title (cliki2::make-random-string 20))
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title)))
         (cliki2::*account* (list "Contributor" nil)))
    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
      (fiveam:is
       (member
        (cliki2::add-revision article "This is an article" "summary")
        (cliki2::revisions (cliki2::find-article article-title))
        :test 'equalp)))))

(fiveam:test add-revision2
  (let* ((article-title (cliki2::make-random-string 20))
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title)))
         (cliki2::*account* (list "Contributor" nil)))
    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
      (fiveam:is
       (member
        (cliki2::add-revision article "This is an article" "summary")
        (cliki2::revisions (cliki2::find-article article-title))
        :test 'equalp))
      (fiveam:is
       (member
        (cliki2::add-revision article "Another revision" "more")
        (cliki2::revisions (cliki2::find-article article-title))
        :test 'equalp))
      (fiveam:is
       (= 2
          (length
           (cliki2::revisions
            (cliki2::find-article article-title))))))))

(fiveam:test add-revision3
  (let* ((article-title (cliki2::make-random-string 20))
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title)))
         (cliki2::*account* (list "Contributor" nil)))
    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
      (fiveam:is
       (member
        (cliki2::add-revision article "This is an article" "summary")
        (cliki2::revisions (cliki2::find-article article-title))
        :test 'equalp))
      (fiveam:is
       (member
        (cliki2::add-revision article "Another revision" "more")
        (cliki2::revisions (cliki2::find-article article-title))
        :test 'equalp))
      (fiveam:is-false (cliki2::deleted? article))
      (fiveam:is
       (member
        (cliki2::add-revision article "" "deleted")
        (cliki2::revisions (cliki2::find-article article-title))
        :test 'equalp))
      (fiveam:is-true (cliki2::deleted? article))
      (fiveam:is
       (member
        (cliki2::add-revision article "Put back" "restored")
        (cliki2::revisions (cliki2::find-article article-title))
        :test 'equalp))
      (fiveam:is-false (cliki2::deleted? article))
      (fiveam:is
       (= 4
          (length
           (cliki2::revisions
            (cliki2::find-article article-title))))))))

;;; edit-link

(fiveam:test edit-link1
  (fiveam:is
   (equal
    "<a href=\"/site/edit-article?title=Some%20Article&amp;from-revision=3565062000\">Edit</a>"
    (cliki2::edit-link
     (list "Some Article"
           3565062000
           ""
           nil
           "Contributor"
           "::1")
     "Edit"))))

;;; article-footer

(fiveam:test article-footer1
  (fiveam:is
   (equal
    "<li><a href=\"/Potato\">Current version</a></li>
      <li><a href=\"/site/history?article=Potato\">History</a></li>
      <li><a href=\"/site/backlinks?article=Potato\">Backlinks</a></li><li><a href=\"/site/edit-article?title=Potato&amp;from-revision=3565062000\">Edit</a></li><li><a href=\"/site/edit-article?create=t\">Create</a></li>"
    (mock 'cliki2::youre-banned? (constantly nil)
      (cliki2::article-footer
       (list "Potato"
             3565062000
             ""
             nil
             "Contributor"
             "::1"))))))

;;; render-revision

(fiveam:test render-revision1
  (fiveam:is
   (equal
    "<div id=\"article\">Very nutritious New World Tuber</div>"
    (let ((cliki2::*footer* nil))
     (mock 'cliki2::youre-banned? (constantly nil)
       (prog1
           (html-output
             (cliki2::render-revision
              (list "Potato"
                    3565062000
                    ""
                    nil
                    "Contributor"
                    "::1")
              "Very nutritious New World Tuber"))
         (fiveam:is
          (equal
           "<li><a href=\"/Potato\">Current version</a></li>
      <li><a href=\"/site/history?article=Potato\">History</a></li>
      <li><a href=\"/site/backlinks?article=Potato\">Backlinks</a></li><li><a href=\"/site/edit-article?title=Potato&amp;from-revision=3565062000\">Edit</a></li><li><a href=\"/site/edit-article?create=t\">Create</a></li>"
           cliki2::*footer*))))))))

;;; find-revision

(fiveam:test find-revision1
  (let* ((article-title (cliki2::make-random-string 20))
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title))))
    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
      (let ((revision (cliki2::add-revision
                       article "Potatoes are delicious" "fact")))
        (fiveam:is
         (equalp
          revision
          (cliki2::find-revision
           article-title
           (write-to-string (cliki2::revision-date revision)))))))))

;;; revision-link

(fiveam:test revision-link1
  (fiveam:is
   (equal
    "/site/view-revision?article=Some%20Article&date=3565062000"
    (cliki2::revision-link
     (list "Some Article"
           3565062000
           ""
           nil
           "Contributor"
           "::1")))))

;;; pprint-revision-link

(fiveam:test pprint-revision-link1
  (fiveam:is
   (equal
    ;; TODO: the & should be escaped as &amp; in href
    "<a class=\"internal\" href=\"/site/view-revision?article=Some%20Article&date=3565062000\">Fri, 21 Dec 2012 07:00:00 GMT</a>"
    (html-output
     (cliki2::pprint-revision-link
      (list "Some Article"
            3565062000
            ""
            nil
            "Contributor"
            "::1"))))))

;;; render-edit-article-common

(test-str render-edit-article-common1
  "<h1>Editing 'Potato'</h1><input type=\"hidden\" name=\"title\" value=\"Potato\" /><textarea rows=\"18\" cols=\"80\" name=\"content\">Quality potato content</textarea>
<dl class=\"prefs\">
<dt><label for=\"summary\">Edit summary:</label></dt>
<dd><input type=\"text\" name=\"summary\" size=\"50\" value=\"doing an edit\" /></dd><dt><label for=\"captcha\">(FLOOR 16 5) is:</label></dt><dd><input class=\"\" name=\"captcha-answer\" size=\"50\" />
     <input type=\"hidden\" name=\"captcha-op\" value=\"FLOOR\" />
     <input type=\"hidden\" name=\"captcha-x\"  value=\"16\" />
     <input type=\"hidden\" name=\"captcha-y\"  value=\"5\" /></dd></dl>
<input type=\"submit\" value=\"Save\" name=\"save\" />
<input type=\"submit\" value=\"Preview\" name=\"preview\" /><h1>Article preview:</h1><div id=\"article\">Quality potato content</div>"
  (html-output
    (let ((cliki2::*title* nil))
      (prog1
          (mock 'cliki2::make-captcha (constantly '(floor 16 5))
            (cliki2::render-edit-article-common
             "Potato" "Quality potato content" "doing an edit"))
        (fiveam:is
         (equal
          "Editing Potato"
          cliki2::*title*))))))

;;; /site/view-revision

(fiveam:test /site/view-revision1
  (let* ((article-title (cliki2::make-random-string 20))
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title))))
    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
      (let* ((revision (cliki2::add-revision
                       article "Potatoes are delicious" "fact")))

        (fiveam:is
         (search
          (format nil "<div class=\"centered\">Revision ~A</div>"
                  (cliki2::rfc-1123-date
                   (cliki2::revision-date revision)))
          (get-request 'cliki2::/site/view-revision
                       "article" article-title
                       "date" (write-to-string
                               (cliki2::revision-date revision)))))))))

;;; /site/edit-article

(fiveam:test /site/edit-article-banned
  (ensure-redirected
      (mock 'cliki2::banned? (constantly t)
        (get-request 'cliki2::/site/edit-article
                     ;; TODO: below should not be needed
                     "title" "bogus"))
      "/"))

(fiveam:test /site/edit-article-create
  (fiveam:is
   (equal
    "<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
    <title>Wooki: Create new article</title>
    <link  rel=\"stylesheet\" href=\"/static/css/style.css\">
    <link  rel=\"stylesheet\" href=\"/static/css/colorize.css\">
  </head>

  <body>
    <span class=\"hidden\">Wooki - Create new article</span>
    <div id=\"content\"><div id=\"content-area\"><form method=\"post\" action=\"/site/edit-article\"><span>Title: </span>
      <input type=\"text\" name=\"title\" size=\"50\" value=\"\"/><textarea rows=\"18\" cols=\"80\" name=\"content\"></textarea>
<dl class=\"prefs\">
<dt><label for=\"summary\">Edit summary:</label></dt>
<dd><input type=\"text\" name=\"summary\" size=\"50\" value=\"created page\" /></dd><dt><label for=\"captcha\">(FLOOR 16 5) is:</label></dt><dd><input class=\"\" name=\"captcha-answer\" size=\"50\" />
     <input type=\"hidden\" name=\"captcha-op\" value=\"FLOOR\" />
     <input type=\"hidden\" name=\"captcha-x\"  value=\"16\" />
     <input type=\"hidden\" name=\"captcha-y\"  value=\"5\" /></dd></dl>
<input type=\"submit\" value=\"Save\" name=\"save\" />
<input type=\"submit\" value=\"Preview\" name=\"preview\" /><h1>Article preview:</h1><div id=\"article\"></div></form></div>
  <div id=\"footer\" class=\"buttonbar\"><ul></ul></div>
  </div>
  <div id=\"header-buttons\" class=\"buttonbar\">
    <ul>
      <li><a href=\"/\">Home</a></li>
      <li><a href=\"/site/recent-changes\">Recent Changes</a></li>
      <li><a href=\"/Wooki\">About</a></li>
      <li><a href=\"/Text%20Formatting\">Text Formatting</a></li>
      <li><a href=\"/site/tools\">Tools</a></li>
    </ul>
    <div id=\"search\">
      <form action=\"/site/search\">
        <label for=\"search_query\" class=\"hidden\">Search Wooki</label>
        <input type=\"text\" name=\"query\" id=\"search_query\" value=\"\" />
        <input type=\"submit\" value=\"search\" />
      </form>
    </div>
  </div>
  <div id=\"pageheader\">
    <div id=\"header\">
      <span id=\"logo\">Wooki</span>
      <span id=\"slogan\">test wiki description</span>
      <div id=\"login\"><form method=\"post\" action=\"/site/login\">
                 <label for=\"login_name\" class=\"hidden\">Account name</label>
                 <input type=\"text\" name=\"name\" id=\"login_name\" class=\"login_input\" />
                 <label for= \"login_password\" class=\"hidden\">Password</label>
                 <input type=\"password\" name=\"password\" id=\"login_password\" class=\"login_input\" />
                 <input type=\"submit\" name=\"login\" value=\"login\" id=\"login_submit\" /><br />
                 <div id=\"register\"><a href=\"/site/register\">register</a></div>
                 <input type=\"submit\" name=\"reset-pw\" value=\"reset password\" id=\"reset_pw\" />
               </form>
      </div>
    </div>
  </div>
  </body></html>"
    (cl-ppcre:regex-replace
     "    \\n" ;; trailing whitespace blank line
     (mock 'cliki2::make-captcha (constantly '(floor 16 5))
       (get-request 'cliki2::/site/edit-article
                    "create" "t"
                    ;; TODO: below should not be needed
                    "title" "bogus"))
     ""))))
