;;;; Copyright 2023 Vladimir Sedach <vsedach@common-lisp.net>

;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public
;;;; License along with this program. If not, see
;;;; <http://www.gnu.org/licenses/>.

(in-package #:cliki2.tests)

(fiveam:def-suite html-rendering-tests :in cliki2-tests)
(fiveam:in-suite html-rendering-tests)

(test-str render-header1
  "<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
    <title>Wooki: Something</title>
    XYZ
    <link  rel=\"stylesheet\" href=\"/static/css/style.css\">
    <link  rel=\"stylesheet\" href=\"/static/css/colorize.css\">
  </head>

  <body>
    <span class=\"hidden\">Wooki - Something</span>
    <div id=\"content\"><div id=\"content-area\">"
  (let ((cliki2::*title* "Something")
        (cliki2::*header* "XYZ"))
    (html-output (cliki2::render-header))))

(test-str render-footer
  "</div>
  <div id=\"footer\" class=\"buttonbar\"><ul></ul></div>
  </div>
  <div id=\"header-buttons\" class=\"buttonbar\">
    <ul>
      <li><a href=\"/\">Home</a></li>
      <li><a href=\"/site/recent-changes\">Recent Changes</a></li>
      <li><a href=\"/Wooki\">About</a></li>
      <li><a href=\"/Text%20Formatting\">Text Formatting</a></li>
      <li><a href=\"/site/tools\">Tools</a></li>
    </ul>
    <div id=\"search\">
      <form action=\"/site/search\">
        <label for=\"search_query\" class=\"hidden\">Search Wooki</label>
        <input type=\"text\" name=\"query\" id=\"search_query\" value=\"XML library\" />
        <input type=\"submit\" value=\"search\" />
      </form>
    </div>
  </div>
  <div id=\"pageheader\">
    <div id=\"header\">
      <span id=\"logo\">Wooki</span>
      <span id=\"slogan\">test wiki description</span>
      <div id=\"login\"><div id=\"logout\">
                 <span><a class=\"internal\" href=\"/site/account?name=Epic%20Poster\">Epic Poster</a></span>
                 <div id=\"logout_button\"><a href=\"/site/logout\">Log out</a></div>
               </div>
      </div>
    </div>
  </div>
  </body></html>"
  (let ((cliki2::*title* "Something")
        (cliki2::*header* "XYZ")
        (cliki2::*account* (list "Epic Poster")))
    (mock 'hunchentoot:get-parameter
        (lambda (next param-name)
          (declare (ignore next))
          (fiveam:is (equal param-name "query"))
          "XML library")
      (html-output (cliki2::render-footer)))))
