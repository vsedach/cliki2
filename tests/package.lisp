(in-package #:common-lisp)

(defpackage #:cliki2.tests
  (:use #:common-lisp)
  (:export
   #:run-tests))
