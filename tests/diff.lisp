;;;; Copyright 2023 Vladimir Sedach <vsedach@common-lisp.net>

;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public
;;;; License along with this program. If not, see
;;;; <http://www.gnu.org/licenses/>.

(in-package #:cliki2.tests)

(fiveam:def-suite diff-tests :in cliki2-tests)
(fiveam:in-suite diff-tests)

(fiveam:test choose-chunks0
  (fiveam:is
   (null (cliki2::choose-chunks (list) :delete :replace :create))))

(fiveam:test compare-strings0
  (multiple-value-bind (original-diff modified-diff)
      (cliki2::compare-strings "" "")
    (fiveam:is (equal "" original-diff))
    (fiveam:is (equal "" modified-diff))))

(fiveam:test path-or-blank0
  (fiveam:is (equal "empty_file"
                    (pathname-name (cliki2::path-or-blank nil)))))

(fiveam:test unified-diff-body0
  ;;todo
  t
  )

(test-str revision-version-info-link0
  "Version <a class=\"internal\" href=\"/site/view-revision?article=Potato&date=3565062000\">Fri, 21 Dec 2012 07:00:00 GMT</a> (<a href=\"/site/edit-article?title=Potato&amp;from-revision=3565062000\">edit</a>)"
  (html-output (cliki2::revision-version-info-links
                (list "Potato"
                      3565062000
                      ""
                      nil
                      "Contributor"
                      "::1"))))

(fiveam:test render-unified-revision-diff0
  (let* ((article-title (cliki2::make-random-string 20))
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title))))
    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
      (let ((revision (cliki2::add-revision
                       article "Potatoes are delicious" "fact")))
        (fiveam:is
         (cl-ppcre:scan
          (cl-ppcre:create-scanner
           "^<div style=\"font-family:monospace;\"><br />--- <br />\\+\\+\\+ Version <a class=\"internal\" href=\"/site/view-revision\\?article=[0-9a-zA-Z]{20}&date=[0-9]{10}\">.+ [0-9]{4} .+ GMT</a> \\(<a href=\"/site/edit-article\\?title=[0-9a-zA-Z]{20}&amp;from-revision=[0-9]{10}\">edit</a>\\)<br /><pre>@@ -1 \\+1,1 @@
\\+Potatoes are delicious
</pre></div>$"
           :single-line-mode t)
          (html-output
            (cliki2::render-unified-revision-diff nil revision))))))))

;;; render-diff-table

(fiveam:test render-diff-table0
  (let* ((article-title (cliki2::make-random-string 20))
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title))))
    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
      (let ((revision (cliki2::add-revision
                       article "Potatoes are delicious" "fact")))
        (fiveam:is
         (cl-ppcre:scan
          (cl-ppcre:create-scanner
           "^<div style=\"display:none;\"><br />
  Unified format diff:<div style=\"font-family:monospace;\"><br />--- <br />\\+\\+\\+ Version <a class=\"internal\" href=\"/site/view-revision\\?article=[0-9a-zA-Z]{20}&date=[0-9]{10}\">.+ [0-9]{4} .+ GMT</a> \\(<a href=\"/site/edit-article\\?title=[0-9a-zA-Z]{20}&amp;from-revision=[0-9]{10}\">edit</a>\\)<br /><pre>@@ -1 \\+1,1 @@
\\+Potatoes are delicious
</pre></div>Table format diff:
  </div>
  <table class=\"diff\">
  <colgroup>
    <col class=\"diff-marker\"> <col class=\"diff-content\">
    <col class=\"diff-marker\"> <col class=\"diff-content\">
  </colgroup>
  <tbody>
    <tr>
      <th colspan=\"2\"></th>
      <th colspan=\"2\">Version <a class=\"internal\" href=\"/site/view-revision\\?article=[0-9a-zA-Z]{20}&date=[0-9]{10}\">.+ [0-9]{4} .+ GMT</a> \\(<a href=\"/site/edit-article\\?title=[0-9a-zA-Z]{20}&amp;from-revision=[0-9]{10}\">edit</a>\\)</th>
    </tr>
    <tr>
  <td /><td class=\"diff-line-number\">Line 0:</td>
  <td /><td class=\"diff-line-number\">Line 0:</td>
</tr><tr><td class=\"diff-marker\" /><td /><td class=\"diff-marker\">\\+</td>
                    <td class=\"diff-addline\" style=\"background-color: #CFC;\">Potatoes are delicious</td></tr>
  </tbody>
  </table>$"
           :single-line-mode t)
          (html-output
            (cliki2::render-diff-table nil revision nil))))))))

;;; /site/compare-revisions

(fiveam:test /site/compare-revisions0
  (let* ((article-title (cliki2::make-random-string 20))
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title))))
    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
      (let* ((revision1 (cliki2::add-revision
                         article "Potatoes are delicious" "fact"))
             (revision2 (cliki2::add-revision
                         article "Potatoes are very delicious"
                         "fixed article"))
             (page (get-request
                    'cliki2::/site/compare-revisions
                    "article" article-title
                    "old" (write-to-string
                           (cliki2::revision-date revision1))
                    "diff" (write-to-string
                            (cliki2::revision-date revision2)))))

         (cl-ppcre:scan
          (cl-ppcre:create-scanner
"^<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
    <title>Wooki: [0-9a-zA-Z]{20} difference between revisions</title>
.*
    <link  rel=\"stylesheet\" href=\"/static/css/style.css\">
    <link  rel=\"stylesheet\" href=\"/static/css/colorize.css\">
  </head>

  <body>
    <span class=\"hidden\">Wooki - [0-9a-zA-Z]{20} difference between revisions</span>
    <div id=\"content\"><div id=\"content-area\"><div class=\"centered\"><h1><a class=\"internal\" href=\"/[0-9a-zA-Z]{20}\">[0-9a-zA-Z]{20}</a></h1></div><div style=\"display:none;\"><br />
  Unified format diff:<div style=\"font-family:monospace;\"><br />--- Version <a class=\"internal\" href=\"/site/view-revision\\?article=[0-9a-zA-Z]{20}&date=[0-9]{10}\">.+ [0-9]{4} .+ GMT</a> \\(<a href=\"/site/edit-article\\?title=[0-9a-zA-Z]{20}&amp;from-revision=[0-9]{10}\">edit</a>\\)<br />\\+\\+\\+ Version <a class=\"internal\" href=\"/site/view-revision\\?article=[0-9a-zA-Z]{20}&date=[0-9]{10}\">.+ [0-9]{4} .+ GMT</a> \\(<a href=\"/site/edit-article\\?title=[0-9a-zA-Z]{20}&amp;from-revision=[0-9]{10}\">edit</a>\\)<br /><pre>@@ -1,1 \\+1,1 @@
-Potatoes are delicious
\\+Potatoes are very delicious
</pre></div>Table format diff:
  </div>
  <table class=\"diff\">
  <colgroup>
    <col class=\"diff-marker\"> <col class=\"diff-content\">
    <col class=\"diff-marker\"> <col class=\"diff-content\">
  </colgroup>
  <tbody>
    <tr>
      <th colspan=\"2\">Version <a class=\"internal\" href=\"/site/view-revision\\?article=[0-9a-zA-Z]{20}&date=[0-9]{10}\">.+ [0-9]{4} .+ GMT</a> \\(<a href=\"/site/edit-article\\?title=[0-9a-zA-Z]{20}&amp;from-revision=[0-9]{10}\">edit</a>\\)</th>
      <th colspan=\"2\">Version <a class=\"internal\" href=\"/site/view-revision\\?article=[0-9a-zA-Z]{20}&date=[0-9]{10}\">.+ [0-9]{4} .+ GMT</a> \\(<a href=\"/site/edit-article\\?title=[0-9a-zA-Z]{20}&amp;from-revision=[0-9]{10}\">edit</a>\\)<form method=\"post\" action=\"/site/undo\">
        <input type=\"hidden\" name=\"article\" value=\"[0-9a-zA-Z]{20}\" /><input type=\"hidden\" name=\"undo-revision\" value=\"[0-9]{10}\" />
    \\(<input type=\"submit\" name=\"undo\" value=\"undo\" class=\"undo\" />\\)</form></th>
    </tr>
    <tr>
  <td /><td class=\"diff-line-number\">Line 0:</td>
  <td /><td class=\"diff-line-number\">Line 0:</td>
</tr><tr><td class=\"diff-marker\">-</td>
                    <td class=\"diff-deleteline\" style=\"background-color: #FFA;\">Potatoes are <span style=\"color:red;\"></span>delicious</td><td class=\"diff-marker\">\\+</td>
                    <td class=\"diff-addline\" style=\"background-color: #CFC;\">Potatoes are <span style=\"color:red;\">very </span>delicious</td></tr>
  </tbody>
  </table></div>
  <div id=\"footer\" class=\"buttonbar\"><ul><li><a href=\"/[0-9a-zA-Z]{20}\">Current version</a></li>
      <li><a href=\"/site/history\\?article=[0-9a-zA-Z]{20}\">History</a></li>
      <li><a href=\"/site/backlinks\\?article=[0-9a-zA-Z]{20}\">Backlinks</a></li><li><a href=\"/site/edit-article\\?title=[0-9a-zA-Z]{20}&amp;from-revision=[0-9]{10}\">Edit</a></li><li><a href=\"/site/edit-article\\?create=t\">Create</a></li></ul></div>
  </div>
  <div id=\"header-buttons\" class=\"buttonbar\">
    <ul>
      <li><a href=\"/\">Home</a></li>
      <li><a href=\"/site/recent-changes\">Recent Changes</a></li>
      <li><a href=\"/Wooki\">About</a></li>
      <li><a href=\"/Text%20Formatting\">Text Formatting</a></li>
      <li><a href=\"/site/tools\">Tools</a></li>
    </ul>
    <div id=\"search\">
      <form action=\"/site/search\">
        <label for=\"search_query\" class=\"hidden\">Search Wooki</label>
        <input type=\"text\" name=\"query\" id=\"search_query\" value=\"\" />
        <input type=\"submit\" value=\"search\" />
      </form>
    </div>
  </div>
  <div id=\"pageheader\">
    <div id=\"header\">
      <span id=\"logo\">Wooki</span>
      <span id=\"slogan\">test wiki description</span>
      <div id=\"login\"><form method=\"post\" action=\"/site/login\">
                 <label for=\"login_name\" class=\"hidden\">Account name</label>
                 <input type=\"text\" name=\"name\" id=\"login_name\" class=\"login_input\" />
                 <label for= \"login_password\" class=\"hidden\">Password</label>
                 <input type=\"password\" name=\"password\" id=\"login_password\" class=\"login_input\" />
                 <input type=\"submit\" name=\"login\" value=\"login\" id=\"login_submit\" /><br />
                 <div id=\"register\"><a href=\"/site/register\">register</a></div>
                 <input type=\"submit\" name=\"reset-pw\" value=\"reset password\" id=\"reset_pw\" />
               </form>
      </div>
    </div>
  </div>
  </body></html>$"
           :single-line-mode t)
          page)))))
