;;;; Copyright 2023 Vladimir Sedach <vsedach@common-lisp.net>

;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public
;;;; License along with this program. If not, see
;;;; <http://www.gnu.org/licenses/>.

(in-package #:cliki2.tests)

(fiveam:def-suite recent-changes-tests :in cliki2-tests)
(fiveam:in-suite recent-changes-tests)

;;; find-previous-revision

(fiveam:test find-previous-revision1
  (let ((mock-revision (list "Article Title"
                             3565062000
                             ""
                             nil
                             "Contributor"
                             "::1")))
    (mock 'cliki2::find-article
        (constantly (list "Article Title" (list mock-revision)))
      (fiveam:is
       (null (cliki2::find-previous-revision mock-revision))))))

(fiveam:test find-previous-revision1
  (let ((revision2 (list "Article Title"
                         3565062001
                         "second"
                         nil
                         "Contributor"
                         "::1"))
        (revision1 (list "Article Title"
                         3565062000
                         "first"
                         nil
                         "Contributor"
                         "::1")))
    (mock 'cliki2::find-article
        (constantly (list "Article Title" (list revision2 revision1)))
      (fiveam:is
       (equalp
        revision1
        (cliki2::find-previous-revision revision2))))))

;;; %render-revision-summary

(fiveam:test %render-revision-summary1
  (let ((mock-revision (list "Potatoes"
                             3565062000
                             "added useful content"
                             nil
                             "Contributor"
                             "::1")))
    (mock 'cliki2::find-article
        (constantly (list "Potatoes" (list mock-revision)))
      (fiveam:is
       (equal
        "<a class=\"internal\" href=\"/site/view-revision?article=Potatoes&date=3565062000\">Fri, 21 Dec 2012 07:00:00 GMT</a> <a class=\"internal\" href=\"/Potatoes\">Potatoes</a>
  - added useful content <a class=\"internal\" href=\"/site/account?name=Contributor\">Contributor</a> "
        (html-output
          (cliki2::%render-revision-summary mock-revision)))))))

(fiveam:test %render-revision-summary2
  (let ((revision2 (list "Potatoes"
                         3565062001
                         "second revision"
                         nil
                         "Contributor"
                         "::1"))
        (revision1 (list "Potatoes"
                         3565062000
                         "added useful content"
                         nil
                         "Contributor"
                         "::1")))
    (mock 'cliki2::find-article
        (constantly (list "Potatoes" (list revision2 revision1)))
      (fiveam:is
       (equal
        "<a class=\"internal\" href=\"/site/view-revision?article=Potatoes&date=3565062001\">Fri, 21 Dec 2012 07:00:01 GMT</a> <a class=\"internal\" href=\"/Potatoes\">Potatoes</a>
  - second revision <a class=\"internal\" href=\"/site/account?name=Contributor\">Contributor</a> (<a class=\"internal\" href=\"/site/compare-revisions?article=Potatoes&old=3565062000&diff=3565062001\">diff</a>)"
        (html-output
          (cliki2::%render-revision-summary revision2)))))))

;;; render-revision-summary

(fiveam:test render-revision-summary1
  (let ((mock-revision (list "Potatoes"
                             3565062000
                             "added useful content"
                             nil
                             "Contributor"
                             "::1")))
    (mock 'cliki2::find-article
        (constantly (list "Potatoes" (list mock-revision)))
      (fiveam:is
       (equal
        "<li><a class=\"internal\" href=\"/site/view-revision?article=Potatoes&date=3565062000\">Fri, 21 Dec 2012 07:00:00 GMT</a> <a class=\"internal\" href=\"/Potatoes\">Potatoes</a>
  - added useful content <a class=\"internal\" href=\"/site/account?name=Contributor\">Contributor</a> </li>"
        (html-output
          (cliki2::render-revision-summary mock-revision)))))))

(fiveam:test render-revision-summary2
  (let ((revision2 (list "Potatoes"
                         3565062001
                         "second revision"
                         nil
                         "Contributor"
                         "::1"))
        (revision1 (list "Potatoes"
                         3565062000
                         "added useful content"
                         nil
                         "Contributor"
                         "::1")))
    (mock 'cliki2::find-article
        (constantly (list "Potatoes" (list revision2 revision1)))
      (fiveam:is
       (equal
        "<li><a class=\"internal\" href=\"/site/view-revision?article=Potatoes&date=3565062001\">Fri, 21 Dec 2012 07:00:01 GMT</a> <a class=\"internal\" href=\"/Potatoes\">Potatoes</a>
  - second revision <a class=\"internal\" href=\"/site/account?name=Contributor\">Contributor</a> (<a class=\"internal\" href=\"/site/compare-revisions?article=Potatoes&old=3565062000&diff=3565062001\">diff</a>)</li>"
        (html-output
          (cliki2::render-revision-summary revision2)))))))

;;; /site/recent-changes

(fiveam:test /site/recent-changes0
  (fiveam:is
   (equal
    "<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
    <title>Wooki: Recent Changes</title>
    <link rel=\"alternate\" type=\"application/atom+xml\" title=\"recent changes\" href=\"/site/feed/recent-changes.atom\">
    <link  rel=\"stylesheet\" href=\"/static/css/style.css\">
    <link  rel=\"stylesheet\" href=\"/static/css/colorize.css\">
  </head>

  <body>
    <span class=\"hidden\">Wooki - Recent Changes</span>
    <div id=\"content\"><div id=\"content-area\"><h1>Recent Changes</h1>
  <a class=\"internal\" href=\"/site/feed/recent-changes.atom\">ATOM feed</a>
  <ul></ul></div>
  <div id=\"footer\" class=\"buttonbar\"><ul></ul></div>
  </div>
  <div id=\"header-buttons\" class=\"buttonbar\">
    <ul>
      <li><a href=\"/\">Home</a></li>
      <li><a href=\"/site/recent-changes\">Recent Changes</a></li>
      <li><a href=\"/Wooki\">About</a></li>
      <li><a href=\"/Text%20Formatting\">Text Formatting</a></li>
      <li><a href=\"/site/tools\">Tools</a></li>
    </ul>
    <div id=\"search\">
      <form action=\"/site/search\">
        <label for=\"search_query\" class=\"hidden\">Search Wooki</label>
        <input type=\"text\" name=\"query\" id=\"search_query\" value=\"\" />
        <input type=\"submit\" value=\"search\" />
      </form>
    </div>
  </div>
  <div id=\"pageheader\">
    <div id=\"header\">
      <span id=\"logo\">Wooki</span>
      <span id=\"slogan\">test wiki description</span>
      <div id=\"login\"><form method=\"post\" action=\"/site/login\">
                 <label for=\"login_name\" class=\"hidden\">Account name</label>
                 <input type=\"text\" name=\"name\" id=\"login_name\" class=\"login_input\" />
                 <label for= \"login_password\" class=\"hidden\">Password</label>
                 <input type=\"password\" name=\"password\" id=\"login_password\" class=\"login_input\" />
                 <input type=\"submit\" name=\"login\" value=\"login\" id=\"login_submit\" /><br />
                 <div id=\"register\"><a href=\"/site/register\">register</a></div>
                 <input type=\"submit\" name=\"reset-pw\" value=\"reset password\" id=\"reset_pw\" />
               </form>
      </div>
    </div>
  </div>
  </body></html>"
    (mock 'cliki2::get-recent-changes (constantly nil)
     (get-request 'cliki2::/site/recent-changes)))))

(fiveam:test /site/recent-changes1
  (fiveam:is
   (equal
    "<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
    <title>Wooki: Recent Changes</title>
    <link rel=\"alternate\" type=\"application/atom+xml\" title=\"recent changes\" href=\"/site/feed/recent-changes.atom\">
    <link  rel=\"stylesheet\" href=\"/static/css/style.css\">
    <link  rel=\"stylesheet\" href=\"/static/css/colorize.css\">
  </head>

  <body>
    <span class=\"hidden\">Wooki - Recent Changes</span>
    <div id=\"content\"><div id=\"content-area\"><h1>Recent Changes</h1>
  <a class=\"internal\" href=\"/site/feed/recent-changes.atom\">ATOM feed</a>
  <ul><li><a class=\"internal\" href=\"/site/view-revision?article=Potatoes&date=3565062001\">Fri, 21 Dec 2012 07:00:01 GMT</a> <a class=\"internal\" href=\"/Potatoes\">Potatoes</a>
  - second revision <a class=\"internal\" href=\"/site/account?name=Contributor\">Contributor</a> (<a class=\"internal\" href=\"/site/compare-revisions?article=Potatoes&old=3565062000&diff=3565062001\">diff</a>)</li><li><a class=\"internal\" href=\"/site/view-revision?article=Potatoes&date=3565062000\">Fri, 21 Dec 2012 07:00:00 GMT</a> <a class=\"internal\" href=\"/Potatoes\">Potatoes</a>
  - added useful content <a class=\"internal\" href=\"/site/account?name=Contributor\">Contributor</a> </li></ul></div>
  <div id=\"footer\" class=\"buttonbar\"><ul></ul></div>
  </div>
  <div id=\"header-buttons\" class=\"buttonbar\">
    <ul>
      <li><a href=\"/\">Home</a></li>
      <li><a href=\"/site/recent-changes\">Recent Changes</a></li>
      <li><a href=\"/Wooki\">About</a></li>
      <li><a href=\"/Text%20Formatting\">Text Formatting</a></li>
      <li><a href=\"/site/tools\">Tools</a></li>
    </ul>
    <div id=\"search\">
      <form action=\"/site/search\">
        <label for=\"search_query\" class=\"hidden\">Search Wooki</label>
        <input type=\"text\" name=\"query\" id=\"search_query\" value=\"\" />
        <input type=\"submit\" value=\"search\" />
      </form>
    </div>
  </div>
  <div id=\"pageheader\">
    <div id=\"header\">
      <span id=\"logo\">Wooki</span>
      <span id=\"slogan\">test wiki description</span>
      <div id=\"login\"><form method=\"post\" action=\"/site/login\">
                 <label for=\"login_name\" class=\"hidden\">Account name</label>
                 <input type=\"text\" name=\"name\" id=\"login_name\" class=\"login_input\" />
                 <label for= \"login_password\" class=\"hidden\">Password</label>
                 <input type=\"password\" name=\"password\" id=\"login_password\" class=\"login_input\" />
                 <input type=\"submit\" name=\"login\" value=\"login\" id=\"login_submit\" /><br />
                 <div id=\"register\"><a href=\"/site/register\">register</a></div>
                 <input type=\"submit\" name=\"reset-pw\" value=\"reset password\" id=\"reset_pw\" />
               </form>
      </div>
    </div>
  </div>
  </body></html>"
    (let ((revision2 (list "Potatoes"
                         3565062001
                         "second revision"
                         nil
                         "Contributor"
                         "::1"))
        (revision1 (list "Potatoes"
                         3565062000
                         "added useful content"
                         nil
                         "Contributor"
                         "::1")))
    (mock 'cliki2::find-article
        (constantly (list "Potatoes" (list revision2 revision1)))
      (mock 'cliki2::get-recent-changes
          (constantly (list revision2 revision1))
        (get-request 'cliki2::/site/recent-changes)))))))

;;; iso8601-time

(test-str iso8601-time1
  "2012-12-21T07:34:21Z"
  (cliki2::iso8601-time 3565064061))

;;; feed-doc

(test-str feed-doc0
  "<?xml version=\"1.0\" encoding=\"utf-8\"?>
    <feed xmlns=\"http://www.w3.org/2005/Atom\">
      <title>XYZ</title>
      <link href=\"http://www.com/foo\" />
      <updated>2012-12-21T07:35:01Z</updated></feed>"
  (let ((hunchentoot:*reply* (make-instance 'hunchentoot:reply)))
    (cliki2::feed-doc "XYZ" "http://www.com/foo" 3565064101
                      (constantly t))))

;;; feed-format-content

(fiveam:test feed-format-content1
  (let* ((article-title "SaladFormat")
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title))))
    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
      (cliki2::add-revision article "Potatoes are delicious" "fact")
      (let ((revision2 (cliki2::add-revision
                        article "Potatoes are very delicious"
                        "fixed article")))

        (fiveam:is
         (equal
          0
          (cl-ppcre:scan
           "^&lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; &lt;a class=&quot;internal&quot; href=&quot;/SaladFormat&quot;&gt;SaladFormat&lt;/a&gt;
  - fixed article &lt;a class=&quot;internal&quot; href=&quot;/site/account\\?name=4.5.6.7&quot;&gt;4.5.6.7&lt;/a&gt; \\(&lt;a class=&quot;internal&quot; href=&quot;/site/compare-revisions\\?article=SaladFormat&amp;old=[0-9]{10}&amp;diff=[0-9]{10}&quot;&gt;diff&lt;/a&gt;\\)&lt;div style=&quot;display:none;&quot;&gt;&lt;br /&gt;
  Unified format diff:&lt;div style=&quot;font-family:monospace;&quot;&gt;&lt;br /&gt;--- Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;br /&gt;\\+\\+\\+ Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;br /&gt;&lt;pre&gt;@@ -1,1 \\+1,1 @@
-Potatoes are delicious
\\+Potatoes are very delicious
&lt;/pre&gt;&lt;/div&gt;Table format diff:
  &lt;/div&gt;
  &lt;table class=&quot;diff&quot;&gt;
  &lt;colgroup&gt;
    &lt;col class=&quot;diff-marker&quot;&gt; &lt;col class=&quot;diff-content&quot;&gt;
    &lt;col class=&quot;diff-marker&quot;&gt; &lt;col class=&quot;diff-content&quot;&gt;
  &lt;/colgroup&gt;
  &lt;tbody&gt;
    &lt;tr&gt;
      &lt;th colspan=&quot;2&quot;&gt;Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;/th&gt;
      &lt;th colspan=&quot;2&quot;&gt;Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;/th&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
  &lt;td /&gt;&lt;td class=&quot;diff-line-number&quot;&gt;Line 0:&lt;/td&gt;
  &lt;td /&gt;&lt;td class=&quot;diff-line-number&quot;&gt;Line 0:&lt;/td&gt;
&lt;/tr&gt;&lt;tr&gt;&lt;td class=&quot;diff-marker&quot;&gt;-&lt;/td&gt;
                    &lt;td class=&quot;diff-deleteline&quot; style=&quot;background-color: #FFA;&quot;&gt;Potatoes are &lt;span style=&quot;color:red;&quot;&gt;&lt;/span&gt;delicious&lt;/td&gt;&lt;td class=&quot;diff-marker&quot;&gt;\\+&lt;/td&gt;
                    &lt;td class=&quot;diff-addline&quot; style=&quot;background-color: #CFC;&quot;&gt;Potatoes are &lt;span style=&quot;color:red;&quot;&gt;very &lt;/span&gt;delicious&lt;/td&gt;&lt;/tr&gt;
  &lt;/tbody&gt;
  &lt;/table&gt;$"
           (cliki2::feed-format-content revision2))))))))

;;; feed-present-revision

(fiveam:test feed-present-revision1
  (let* ((article-title "SaladFormat")
         (article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article :article-title article-title))))
    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
      (cliki2::add-revision article "Potatoes are delicious" "fact")
      (let ((revision2 (cliki2::add-revision
                        article "Potatoes are very delicious"
                        "fixed article")))

        (fiveam:is
         (equal
          0
          (cl-ppcre:scan
           "^<entry>
  <title>SaladFormat - fixed article 4.5.6.7</title>
  <link href=\"/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}\" type=\"text/html\" />
  <updated>.+</updated>
  <content type=\"html\">&lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; &lt;a class=&quot;internal&quot; href=&quot;/SaladFormat&quot;&gt;SaladFormat&lt;/a&gt;
  - fixed article &lt;a class=&quot;internal&quot; href=&quot;/site/account\\?name=4.5.6.7&quot;&gt;4.5.6.7&lt;/a&gt; \\(&lt;a class=&quot;internal&quot; href=&quot;/site/compare-revisions\\?article=SaladFormat&amp;old=[0-9]{10}&amp;diff=[0-9]{10}&quot;&gt;diff&lt;/a&gt;\\)&lt;div style=&quot;display:none;&quot;&gt;&lt;br /&gt;
  Unified format diff:&lt;div style=&quot;font-family:monospace;&quot;&gt;&lt;br /&gt;--- Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;br /&gt;\\+\\+\\+ Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;br /&gt;&lt;pre&gt;@@ -1,1 \\+1,1 @@
-Potatoes are delicious
\\+Potatoes are very delicious
&lt;/pre&gt;&lt;/div&gt;Table format diff:
  &lt;/div&gt;
  &lt;table class=&quot;diff&quot;&gt;
  &lt;colgroup&gt;
    &lt;col class=&quot;diff-marker&quot;&gt; &lt;col class=&quot;diff-content&quot;&gt;
    &lt;col class=&quot;diff-marker&quot;&gt; &lt;col class=&quot;diff-content&quot;&gt;
  &lt;/colgroup&gt;
  &lt;tbody&gt;
    &lt;tr&gt;
      &lt;th colspan=&quot;2&quot;&gt;Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;/th&gt;
      &lt;th colspan=&quot;2&quot;&gt;Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;/th&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
  &lt;td /&gt;&lt;td class=&quot;diff-line-number&quot;&gt;Line 0:&lt;/td&gt;
  &lt;td /&gt;&lt;td class=&quot;diff-line-number&quot;&gt;Line 0:&lt;/td&gt;
&lt;/tr&gt;&lt;tr&gt;&lt;td class=&quot;diff-marker&quot;&gt;-&lt;/td&gt;
                    &lt;td class=&quot;diff-deleteline&quot; style=&quot;background-color: #FFA;&quot;&gt;Potatoes are &lt;span style=&quot;color:red;&quot;&gt;&lt;/span&gt;delicious&lt;/td&gt;&lt;td class=&quot;diff-marker&quot;&gt;\\+&lt;/td&gt;
                    &lt;td class=&quot;diff-addline&quot; style=&quot;background-color: #CFC;&quot;&gt;Potatoes are &lt;span style=&quot;color:red;&quot;&gt;very &lt;/span&gt;delicious&lt;/td&gt;&lt;/tr&gt;
  &lt;/tbody&gt;
  &lt;/table&gt;</content>
</entry>$"
           (html-output
            (cliki2::feed-present-revision revision2)))))))))

;;; /site/feed/recent-changes.atom

(fiveam:test /site/feed/recent-changes.atom
  (fiveam:is
   (equal
    0
    (cl-ppcre:scan
     "^<\\?xml version=\"1.0\" encoding=\"utf-8\"\\?>
    <feed xmlns=\"http://www.w3.org/2005/Atom\">
      <title>Wooki Recent Changes</title>
      <link href=\"/site/feed/recent-changes.atom\" />
      <updated>.+Z</updated><entry>
  <title>SaladFormat - fixed article 4.5.6.7</title>
  <link href=\"/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}\" type=\"text/html\" />
  <updated>.+Z</updated>
  <content type=\"html\">&lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; &lt;a class=&quot;internal&quot; href=&quot;/SaladFormat&quot;&gt;SaladFormat&lt;/a&gt;
  - fixed article &lt;a class=&quot;internal&quot; href=&quot;/site/account\\?name=4.5.6.7&quot;&gt;4.5.6.7&lt;/a&gt; \\(&lt;a class=&quot;internal&quot; href=&quot;/site/compare-revisions\\?article=SaladFormat&amp;old=[0-9]{10}&amp;diff=[0-9]{10}&quot;&gt;diff&lt;/a&gt;\\)&lt;div style=&quot;display:none;&quot;&gt;&lt;br /&gt;
  Unified format diff:&lt;div style=&quot;font-family:monospace;&quot;&gt;&lt;br /&gt;--- Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;br /&gt;\\+\\+\\+ Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;br /&gt;&lt;pre&gt;@@ -1,1 \\+1,1 @@
-Potatoes are delicious
\\+Potatoes are very delicious
&lt;/pre&gt;&lt;/div&gt;Table format diff:
  &lt;/div&gt;
  &lt;table class=&quot;diff&quot;&gt;
  &lt;colgroup&gt;
    &lt;col class=&quot;diff-marker&quot;&gt; &lt;col class=&quot;diff-content&quot;&gt;
    &lt;col class=&quot;diff-marker&quot;&gt; &lt;col class=&quot;diff-content&quot;&gt;
  &lt;/colgroup&gt;
  &lt;tbody&gt;
    &lt;tr&gt;
      &lt;th colspan=&quot;2&quot;&gt;Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;/th&gt;
      &lt;th colspan=&quot;2&quot;&gt;Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;/th&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
  &lt;td /&gt;&lt;td class=&quot;diff-line-number&quot;&gt;Line 0:&lt;/td&gt;
  &lt;td /&gt;&lt;td class=&quot;diff-line-number&quot;&gt;Line 0:&lt;/td&gt;
&lt;/tr&gt;&lt;tr&gt;&lt;td class=&quot;diff-marker&quot;&gt;-&lt;/td&gt;
                    &lt;td class=&quot;diff-deleteline&quot; style=&quot;background-color: #FFA;&quot;&gt;Potatoes are &lt;span style=&quot;color:red;&quot;&gt;&lt;/span&gt;delicious&lt;/td&gt;&lt;td class=&quot;diff-marker&quot;&gt;\\+&lt;/td&gt;
                    &lt;td class=&quot;diff-addline&quot; style=&quot;background-color: #CFC;&quot;&gt;Potatoes are &lt;span style=&quot;color:red;&quot;&gt;very &lt;/span&gt;delicious&lt;/td&gt;&lt;/tr&gt;
  &lt;/tbody&gt;
  &lt;/table&gt;</content>
</entry><entry>
  <title>SaladFormat - fact 4.5.6.7</title>
  <link href=\"/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}\" type=\"text/html\" />
  <updated>.+Z</updated>
  <content type=\"html\">&lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; &lt;a class=&quot;internal&quot; href=&quot;/SaladFormat&quot;&gt;SaladFormat&lt;/a&gt;
  - fact &lt;a class=&quot;internal&quot; href=&quot;/site/account\\?name=4.5.6.7&quot;&gt;4.5.6.7&lt;/a&gt; &lt;div style=&quot;display:none;&quot;&gt;&lt;br /&gt;
  Unified format diff:&lt;div style=&quot;font-family:monospace;&quot;&gt;&lt;br /&gt;--- &lt;br /&gt;\\+\\+\\+ Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;br /&gt;&lt;pre&gt;@@ -1 \\+1,1 @@
\\+Potatoes are delicious
&lt;/pre&gt;&lt;/div&gt;Table format diff:
  &lt;/div&gt;
  &lt;table class=&quot;diff&quot;&gt;
  &lt;colgroup&gt;
    &lt;col class=&quot;diff-marker&quot;&gt; &lt;col class=&quot;diff-content&quot;&gt;
    &lt;col class=&quot;diff-marker&quot;&gt; &lt;col class=&quot;diff-content&quot;&gt;
  &lt;/colgroup&gt;
  &lt;tbody&gt;
    &lt;tr&gt;
      &lt;th colspan=&quot;2&quot;&gt;&lt;/th&gt;
      &lt;th colspan=&quot;2&quot;&gt;Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;/th&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
  &lt;td /&gt;&lt;td class=&quot;diff-line-number&quot;&gt;Line 0:&lt;/td&gt;
  &lt;td /&gt;&lt;td class=&quot;diff-line-number&quot;&gt;Line 0:&lt;/td&gt;
&lt;/tr&gt;&lt;tr&gt;&lt;td class=&quot;diff-marker&quot; /&gt;&lt;td /&gt;&lt;td class=&quot;diff-marker&quot;&gt;\\+&lt;/td&gt;
                    &lt;td class=&quot;diff-addline&quot; style=&quot;background-color: #CFC;&quot;&gt;Potatoes are delicious&lt;/td&gt;&lt;/tr&gt;
  &lt;/tbody&gt;
  &lt;/table&gt;</content>
</entry></feed>$"
     (let* ((article-title "SaladFormat")
            (article
              (cliki2::wiki-new
               'cliki2::article
               (cliki2::make-article :article-title article-title))))
       (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
         (let ((revision1 (cliki2::add-revision
                           article "Potatoes are delicious"
                           "fact"))
               (revision2 (cliki2::add-revision
                           article "Potatoes are very delicious"
                           "fixed article")))
           (mock 'cliki2::get-recent-changes
               (constantly (list revision2 revision1))
             (let ((hunchentoot:*reply*
                     (make-instance 'hunchentoot:reply)))
               (mock 'plausible-cliki-page? (constantly t)
                 (get-request
                  'cliki2::/site/feed/recent-changes.atom)))))))))))

;;; /site/feed/article.atom

(fiveam:test /site/feed/article.atom
  (fiveam:is
   (equal
    0
    (cl-ppcre:scan
     "<\\?xml version=\"1.0\" encoding=\"utf-8\"\\?>
    <feed xmlns=\"http://www.w3.org/2005/Atom\">
      <title>Wooki Article SaladFormat2 Edits</title>
      <link href=\"/site/feed/article.atom\\?title=SaladFormat2\" />
      <updated>.+Z</updated><entry>
  <title>SaladFormat2 - fixed article 4.5.6.7</title>
  <link href=\"/site/view-revision\\?article=SaladFormat2&amp;date=[0-9]{10}\" type=\"text/html\" />
  <updated>.+Z</updated>
  <content type=\"html\">&lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat2&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; &lt;a class=&quot;internal&quot; href=&quot;/SaladFormat2&quot;&gt;SaladFormat2&lt;/a&gt;
  - fixed article &lt;a class=&quot;internal&quot; href=&quot;/site/account\\?name=4.5.6.7&quot;&gt;4.5.6.7&lt;/a&gt; \\(&lt;a class=&quot;internal&quot; href=&quot;/site/compare-revisions\\?article=SaladFormat2&amp;old=[0-9]{10}&amp;diff=[0-9]{10}&quot;&gt;diff&lt;/a&gt;\\)&lt;div style=&quot;display:none;&quot;&gt;&lt;br /&gt;
  Unified format diff:&lt;div style=&quot;font-family:monospace;&quot;&gt;&lt;br /&gt;--- Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat2&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat2&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;br /&gt;\\+\\+\\+ Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat2&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat2&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;br /&gt;&lt;pre&gt;@@ -1,1 \\+1,1 @@
-Potatoes are delicious
\\+Potatoes are very delicious
&lt;/pre&gt;&lt;/div&gt;Table format diff:
  &lt;/div&gt;
  &lt;table class=&quot;diff&quot;&gt;
  &lt;colgroup&gt;
    &lt;col class=&quot;diff-marker&quot;&gt; &lt;col class=&quot;diff-content&quot;&gt;
    &lt;col class=&quot;diff-marker&quot;&gt; &lt;col class=&quot;diff-content&quot;&gt;
  &lt;/colgroup&gt;
  &lt;tbody&gt;
    &lt;tr&gt;
      &lt;th colspan=&quot;2&quot;&gt;Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat2&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat2&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;/th&gt;
      &lt;th colspan=&quot;2&quot;&gt;Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat2&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat2&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;/th&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
  &lt;td /&gt;&lt;td class=&quot;diff-line-number&quot;&gt;Line 0:&lt;/td&gt;
  &lt;td /&gt;&lt;td class=&quot;diff-line-number&quot;&gt;Line 0:&lt;/td&gt;
&lt;/tr&gt;&lt;tr&gt;&lt;td class=&quot;diff-marker&quot;&gt;-&lt;/td&gt;
                    &lt;td class=&quot;diff-deleteline&quot; style=&quot;background-color: #FFA;&quot;&gt;Potatoes are &lt;span style=&quot;color:red;&quot;&gt;&lt;/span&gt;delicious&lt;/td&gt;&lt;td class=&quot;diff-marker&quot;&gt;\\+&lt;/td&gt;
                    &lt;td class=&quot;diff-addline&quot; style=&quot;background-color: #CFC;&quot;&gt;Potatoes are &lt;span style=&quot;color:red;&quot;&gt;very &lt;/span&gt;delicious&lt;/td&gt;&lt;/tr&gt;
  &lt;/tbody&gt;
  &lt;/table&gt;</content>
</entry><entry>
  <title>SaladFormat2 - fact 4.5.6.7</title>
  <link href=\"/site/view-revision\\?article=SaladFormat2&amp;date=[0-9]{10}\" type=\"text/html\" />
  <updated>.+Z</updated>
  <content type=\"html\">&lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat2&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; &lt;a class=&quot;internal&quot; href=&quot;/SaladFormat2&quot;&gt;SaladFormat2&lt;/a&gt;
  - fact &lt;a class=&quot;internal&quot; href=&quot;/site/account\\?name=4.5.6.7&quot;&gt;4.5.6.7&lt;/a&gt; &lt;div style=&quot;display:none;&quot;&gt;&lt;br /&gt;
  Unified format diff:&lt;div style=&quot;font-family:monospace;&quot;&gt;&lt;br /&gt;--- &lt;br /&gt;\\+\\+\\+ Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat2&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat2&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;br /&gt;&lt;pre&gt;@@ -1 \\+1,1 @@
\\+Potatoes are delicious
&lt;/pre&gt;&lt;/div&gt;Table format diff:
  &lt;/div&gt;
  &lt;table class=&quot;diff&quot;&gt;
  &lt;colgroup&gt;
    &lt;col class=&quot;diff-marker&quot;&gt; &lt;col class=&quot;diff-content&quot;&gt;
    &lt;col class=&quot;diff-marker&quot;&gt; &lt;col class=&quot;diff-content&quot;&gt;
  &lt;/colgroup&gt;
  &lt;tbody&gt;
    &lt;tr&gt;
      &lt;th colspan=&quot;2&quot;&gt;&lt;/th&gt;
      &lt;th colspan=&quot;2&quot;&gt;Version &lt;a class=&quot;internal&quot; href=&quot;/site/view-revision\\?article=SaladFormat2&amp;date=[0-9]{10}&quot;&gt;.+ GMT&lt;/a&gt; \\(&lt;a href=&quot;/site/edit-article\\?title=SaladFormat2&amp;amp;from-revision=[0-9]{10}&quot;&gt;edit&lt;/a&gt;\\)&lt;/th&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
  &lt;td /&gt;&lt;td class=&quot;diff-line-number&quot;&gt;Line 0:&lt;/td&gt;
  &lt;td /&gt;&lt;td class=&quot;diff-line-number&quot;&gt;Line 0:&lt;/td&gt;
&lt;/tr&gt;&lt;tr&gt;&lt;td class=&quot;diff-marker&quot; /&gt;&lt;td /&gt;&lt;td class=&quot;diff-marker&quot;&gt;\\+&lt;/td&gt;
                    &lt;td class=&quot;diff-addline&quot; style=&quot;background-color: #CFC;&quot;&gt;Potatoes are delicious&lt;/td&gt;&lt;/tr&gt;
  &lt;/tbody&gt;
  &lt;/table&gt;</content>
</entry></feed>"
     (let* ((article-title "SaladFormat2")
            (article
              (cliki2::wiki-new
               'cliki2::article
               (cliki2::make-article :article-title article-title))))
       (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
         (let ((revision1 (cliki2::add-revision
                           article "Potatoes are delicious"
                           "fact"))
               (revision2 (cliki2::add-revision
                           article "Potatoes are very delicious"
                           "fixed article")))
           (fiveam:is
            (equalp (list revision2 revision1)
                    (cliki2::revisions (cliki2::find-article article-title))))
           (let ((hunchentoot:*reply*
                   (make-instance 'hunchentoot:reply)))
             (mock 'plausible-cliki-page? (constantly t)
               (get-request
                'cliki2::/site/feed/article.atom
                "title" "SaladFormat2"))))))))))
