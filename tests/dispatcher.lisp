;;;; Copyright 2023 Vladimir Sedach <vsedach@common-lisp.net>

;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public
;;;; License along with this program. If not, see
;;;; <http://www.gnu.org/licenses/>.

(in-package #:cliki2.tests)

(fiveam:def-suite dispatcher-tests :in cliki2-tests)
(fiveam:in-suite dispatcher-tests)

(test-str guess-article-name0
  "foobar/xyz"
  (mock 'hunchentoot:script-name* (constantly "/foobar/xyz")
    (cliki2::guess-article-name)))

(fiveam:test show-deleted-article-page1
  (let ((hunchentoot:*reply* (make-instance 'hunchentoot:reply))
        (cliki2::*footer* ""))
    (fiveam:is
     (equal "Article was deleted"
            (html-output
              (cliki2::show-deleted-article-page
               (list (cliki2::make-random-string 20) (list))))))
    (fiveam:is
     (= 404 (hunchentoot::return-code hunchentoot:*reply*)))))

;;; render-article is covered by other tests

;;; dispatchers need to be covered by integration tests
