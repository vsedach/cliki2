;;;; Copyright 2019, 2023 Vladimir Sedach <vsedach@common-lisp.net>

;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public
;;;; License along with this program. If not, see
;;;; <http://www.gnu.org/licenses/>.

(in-package #:cliki2.tests)

(fiveam:def-suite markup-tests :in cliki2-tests)
(fiveam:in-suite markup-tests)

;;; generate-html-from-markup

(test-str generate-html-from-markup0
  "<div id=\"article\"></div>"
  (html-output
    (cliki2::generate-html-from-markup "")))

(test-str generate-html-from-markup1
  "<div id=\"article\">nonsense</div>"
  (html-output
    (cliki2::generate-html-from-markup "nonsense")))

(test-str generate-html-from-markup-pre1
  "<div id=\"article\">nonsense <pre>stuff here</pre> whatever</div>"
  (html-output
    (cliki2::generate-html-from-markup
     "nonsense <pre>stuff here</pre> whatever")))

(test-str generate-html-from-markup-pre2
  "<div id=\"article\">nonsense <pre>_(abc) *(123) /(xyz) _H(defun) _P(http://site.com/package.tgz)</pre> whatever</div>"
  (html-output
    (cliki2::generate-html-from-markup
     "nonsense <pre>_(abc) *(123) /(xyz) _H(defun) _P(http://site.com/package.tgz)</pre> whatever")))

(test-str generate-html-from-markup-code1
  "<div id=\"article\">nonsense <div class=\"code\"><span class=\"nonparen\">code here</span></div> whatever</div>"
  (html-output
    (cliki2::generate-html-from-markup
     "nonsense <code lang=\"c\">code here</code> whatever")))

;;; parse-markup-fragment

(defun parse-mf (str)
  (cliki2::parse-markup-fragment str 0 (length str)))

(test-str parse-markup-fragment0
  ""
  (parse-mf ""))

(test-str parse-markup-fragment1
  "stuff"
  (parse-mf "stuff"))

(test-str parse-markup-fragment2
  "<p>"
  (parse-mf "

"))

(test-str parse-markup-fragment-less-than
  "X &lt; Y"
  (parse-mf "X < Y"))

;; TODO check sanitizer
;; (fiveam:test parse-markup-fragment-sanitize)

;;; escape-pre-block

(defun escape-pb (str)
  (cliki2::escape-pre-block str 0 (length str)))

(test-str escape-pre-block0
  ""
  (escape-pb ""))

(test-str escape-pre-block1
  "foo"
  (escape-pb "foo"))

(test-str escape-pre-block2
  "<pre></pre>"
  (escape-pb "<PRE></PRE>"))

(test-str escape-pre-block3
  "<pre>words</pre>"
  (escape-pb "<pre>words</pre>"))

(test-str escape-pre-block-newline
  "<pre>words


  why?</pre>"
  (escape-pb "<pre>words


  why?</pre>"))

(test-str escape-pre-block-cliki-markup
  "<pre>_(abc) *(123) /(xyz) _H(defun) _P(http://site.com/package.tgz)</pre>"
  (escape-pb "<pre>_(abc) *(123) /(xyz) _H(defun) _P(http://site.com/package.tgz)</pre>"))

(test-str escape-pre-block-html
  "<pre>is html &lt;hr /&gt; ok? is x &lt; y? &amp; is A &gt; B?</pre>"
  (escape-pb "<pre>is html <hr /> ok? is x < y? & is A > B?</pre>"))

;;; escape-parens-in-href-links

(defun escape-parens (str)
  (cliki2::escape-parens-in-href-links str 0 (length str)))

(test-str escape-parens-in-href-links0
  ""
  (escape-parens ""))

(test-str escape-parens-in-href-links1
  "<a href=\"http://www.com/foo%28bar%29\">abc(xyz)</a>"
  (escape-parens "<a href=\"http://www.com/foo(bar)\">abc(xyz)</a>"))

;;; parse-cliki-markup

(defmacro markup-test (name expected markup)
  `(test-str ,name
     ,expected
     (cliki2::parse-cliki-markup ,markup)))

(markup-test parse-internal-link1
  "<a href=\"/Foo%20bar\" class=\"new\">Foo bar</a>"
  "_(Foo bar)")

(markup-test parse-internal-link2
  "<a href=\"/Something%20else\" class=\"new\">Shortname</a>"
  "_(Something else|Shortname)")

(markup-test parse-internal-link3
  "<a href=\"/Foo%20bar\" class=\"new\">Foo bar</a>)))"
  "_(Foo bar))))")

(markup-test parse-empty-internal-link
  "<a href=\"/\" class=\"new\"></a>"
  "_()")

(markup-test parse-empty-internal-link1
  "<a href=\"/\" class=\"new\"></a>"
  "_(|)")

(markup-test parse-empty-internal-link2
  "<a href=\"/\" class=\"new\">x</a>"
  "_(|x)")

(markup-test parse-empty-internal-link3
  "<a href=\"/z\" class=\"new\">x y</a>"
  "_(z|x y)")

(test-str parse-hyperspec-link
  "<a href=\"NIL\" class=\"hyperspec\">setf</a>"
  ;; colorize clhs-lookup warns when local hyperspec path
  ;; has not been configured by writing to *trace-output*
  ;; instead of signaling
  (let ((*trace-output* (make-broadcast-stream)))
    (cliki2::parse-cliki-markup "_H(setf)")))

(markup-test parse-topic-link
  "<a href=\"/fruit\" class=\"new\">fruit</a>"
  "*(fruit)")

(markup-test parse-topic-list
  "<ul></ul>"
  "/(list of lists)")

(markup-test parse-empty-topic-list
  "<ul></ul>"
  "/()")

(markup-test bunch-of-links
  "<a href=\"/foo\" class=\"new\">foo</a> something <a href=\"/bar\" class=\"new\">bar</a> <a href='http://common-lisp.net'>somewhere</a>
 <ul></ul>"
  "_(foo) something *(bar) <a href='http://common-lisp.net'>somewhere</a>
 /(baz)")

(test-str escape-parens-in-href-links
  "*(baz) <a href=\"foo %28bar%29\">foo (bar)</a> _(not bar)"
  (cliki2::escape-parens-in-href-links
   "*(baz) <a href=\"foo (bar)\">foo (bar)</a> _(not bar)" 0 51))

(markup-test parse-article-link-with-escaped-parentheses
  "<a href=\"/Foo%20%28bar%29\" class=\"new\">Foo (bar)</a>"
  "_(Foo (bar\\))")

(markup-test parse-article-link-with-escaped-parentheses1
  "<a href=\"/Foo%20%28bar%29%20baz\" class=\"new\">Foo (bar) baz</a>"
  "_(Foo (bar\\) baz)")

(markup-test parse-article-link-with-escaped-parentheses2
  "<a href=\"/Foo%20%28bar%29\" class=\"new\">Foo (bar)</a>)"
  "_(Foo (bar\\)))")

(markup-test parse-empty-article
  ""
  "")

(markup-test parse-short-article
  "a"
  "a")

(markup-test parse-short-article1
  "ab"
  "ab")

(markup-test parse-short-article2
  "abc"
  "abc")

(markup-test parse-short-article3
  "("
  "(")

(markup-test parse-short-article4
  "_("
  "_(")

(markup-test parse-short-article5
  ")"
  ")")

(markup-test parse-short-article6
  "()"
  "()")

(markup-test parse-some-article
  "something <a href=\"/Foo%20bar\" class=\"new\">Foo bar</a> baz"
  "something _(Foo bar) baz")

(markup-test parse-some-article1
  "(abc <a href=\"/Foo%20bar\" class=\"new\">Foo bar</a> xyz)"
  "(abc _(Foo bar) xyz)")

(markup-test parse-some-article2
  "abc <a href=\"/Foo%20bar\" class=\"new\">Foo bar</a> (xyz"
  "abc _(Foo bar) (xyz")

;;; pprint-article-underscore-link

(test-str pprint-article-underscore-link0
  "<a href=\"/\" class=\"new\"></a>" ; probably should be something else
  (html-output
    (cliki2::pprint-article-underscore-link "")))

(test-str pprint-article-underscore-link1
  "<a href=\"/Many%20Potatoes\" class=\"new\">Many Potatoes</a>"
  (html-output
    (cliki2::pprint-article-underscore-link "Many Potatoes")))

(test-str pprint-article-underscore-link2
  "<a href=\"/Many%20Potatoes\" class=\"new\">tomato</a>"
  (html-output
    (cliki2::pprint-article-underscore-link "Many Potatoes|tomato")))

(test-str pprint-article-underscore-link3
  "<a href=\"/Many%20Potatoes\" class=\"new\">spicy tomato</a>"
  (html-output
    (cliki2::pprint-article-underscore-link
     "Many Potatoes|spicy tomato")))

;;; article-description

(test-str article-description0
  ""
  (mock 'cliki2::cached-content
      (constantly "")
    (cliki2::article-description "mock")))

(test-str article-description1
  "potato"
  (mock 'cliki2::cached-content
      (constantly "potato")
    (cliki2::article-description "mock")))

(test-str article-description2
  "crunchy potato"
  (mock 'cliki2::cached-content
      (constantly "crunchy potato")
    (cliki2::article-description "mock")))

(test-str article-description3
  ""
  (mock 'cliki2::cached-content
      (constantly "
crunchy tomato")
    (cliki2::article-description "mock")))

(test-str article-description4
  "yams"
  (mock 'cliki2::cached-content
      (constantly "yams
crunchy tomato")
    (cliki2::article-description "mock")))

(test-str article-description5
  "crunchy tomato"
  (mock 'cliki2::cached-content
      (constantly "crunchy tomato. not carrot")
    (cliki2::article-description "mock")))

(test-str article-description6
  "crunchy.tomato is delicious"
  (mock 'cliki2::cached-content
      (constantly "crunchy.tomato is delicious")
    (cliki2::article-description "mock")))

(test-str article-description7
  "crunchy.tomato is delicious.yes"
  (mock 'cliki2::cached-content
      (constantly "crunchy.tomato is delicious.yes. why?")
    (cliki2::article-description "mock")))

;;; pprint-article-summary-li

(test-str pprint-article-summary-li0
  (concatenate
   'string
   "<li><a href=\"/mock\" class=\"new\">mock</a> -
   ""
  </li>")

  (html-output
    (mock 'cliki2::cached-content
        (constantly "")
      (cliki2::pprint-article-summary-li "mock" "-"))))

(test-str pprint-article-summary-li1
  (concatenate
   'string
   "<li><a href=\"/mock\" class=\"new\">mock</a> -
   crunchy.tomato is delicious.yes ""
  </li>")

  (html-output
    (mock 'cliki2::cached-content
        (constantly "crunchy.tomato is delicious.yes. why?")
      (cliki2::pprint-article-summary-li "mock" "-"))))

;;; format-topic-list

(test-str format-topic-list0
  "<ul></ul>"
  (html-output
    (cliki2::format-topic-list "NoSuchTopic")))

(fiveam:test format-topic-list1
  (let ((article1
          (cliki2::wiki-new
           'cliki2::article
           (cliki2::make-article :article-title "Cauliflower")))
        (article2
          (cliki2::wiki-new
           'cliki2::article
           (cliki2::make-article :article-title "Carrots"))))

    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
      (cliki2::add-revision
       article1
       "Cauliflower is a fruit
*(fruit)"
       "created page")
      (cliki2::add-revision
       article2
       "Eat more carrots
*(fruit)"
       "created page"))

    (fiveam:is
     (equal
      (concatenate
       'string
       "<ul><li><a href=\"/Carrots\" class=\"internal\">Carrots</a> -
   Eat more carrots ""
  </li><li><a href=\"/Cauliflower\" class=\"internal\">Cauliflower</a> -
   Cauliflower is a fruit ""
  </li></ul>")
      (html-output
       (cliki2::format-topic-list "fruit"))))))

;;; format-hyperspec-link

(test-str format-hyperspec-link1
  "<a href=\"https://cliki.net/site/HyperSpec/Body/mac_destructuring-bind.html\" class=\"hyperspec\">destructuring-bind</a>"
  (html-output
    (mock 'clhs-lookup:spec-lookup
        (constantly
         "https://cliki.net/site/HyperSpec/Body/mac_destructuring-bind.html")
      (cliki2::format-hyperspec-link "destructuring-bind"))))

;;; format-package-link

(test-str format-package-link1
  "<a href=\"http://site.com/package.tgz\">ASDF-install package (obsolete) http://site.com/package.tgz</a>"
  (html-output
    (cliki2::format-package-link "http://site.com/package.tgz")))

;;; markup-code

(defun code-salad (x)
  (cliki2::markup-code x 0 (length x)))

(test-str markup-code0
  ""
  (code-salad ""))

(test-str markup-code1
  "not code"
  (code-salad "not code"))

(test-str markup-code2
  "<code></code>"
  (code-salad "<CODE></CODE>"))

(test-str markup-code3
  "<code>abc</code>"
  (code-salad "<CODE>abc</CODE>"))

(test-str markup-code4
  "<div class=\"code\"><span class=\"nonparen\"></span></div>"
  (code-salad "<CODE lang=\"lisp\"></CODE>"))

(test-str markup-code5
  "<div class=\"code\"><span class=\"nonparen\">abc</span></div>"
  (code-salad "<CODE lang=\"lisp\">abc</CODE>"))

(test-str markup-code6
  "<div class=\"code\"><span class=\"nonparen\"><span class=\"paren1\">(<span class=\"nonparen\"><i><span class=\"symbol\">defun</span></i> make-salad <span class=\"paren2\">(<span class=\"nonparen\">lettuce tomato</span>)</span>
  <span class=\"paren2\">(<span class=\"nonparen\">mix <span class=\"paren3\">(<span class=\"nonparen\">chop lettuce</span>)</span> <span class=\"paren3\">(<span class=\"nonparen\">slice tomato</span>)</span></span>)</span></span>)</span></span></div>"
  (code-salad "<CODE lang=\"lisp\">(defun make-salad (lettuce tomato)
  (mix (chop lettuce) (slice tomato)))</CODE>"))
