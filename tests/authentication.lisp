;;;; Copyright 2023 Vladimir Sedach <vsedach@common-lisp.net>

;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public
;;;; License along with this program. If not, see
;;;; <http://www.gnu.org/licenses/>.

(in-package #:cliki2.tests)

(fiveam:def-suite authentication-tests :in cliki2-tests)
(fiveam:in-suite authentication-tests)

(fiveam:test logout1
  (let ((cookie-secret (cliki2::make-random-string 20))
        (hunchentoot:*reply* (make-instance 'hunchentoot:reply)))
    (setf (gethash cookie-secret (cliki2::sessions cliki2::*wiki*))
          t)
    (mock 'hunchentoot:cookie-in (constantly cookie-secret)
      (cliki2::logout)
      (fiveam:is-false
       (gethash cookie-secret (cliki2::sessions cliki2::*wiki*))))))

(fiveam:test expire-old-sessions1
  (let ((wiki (mock 'cliki2::get-directory-lock (constantly nil)
                (cliki2::make-wiki
                 "XYZ"
                 "something"
                 (ensure-directories-exist
                  (merge-pathnames
                   (make-pathname :directory
                                  (list :relative
                                        "cliki2test"
                                        (cliki2::make-random-string 10)))
                   (uiop:temporary-directory)))
                 "wiki@domain.com"))))
    (flet ((make-sesh (time-thunk)
             (map-into
              (make-array (random 50))
              (lambda ()
                (let ((sesh (cliki2::make-session
                             :expires-at (funcall time-thunk))))
                  (setf (gethash sesh (cliki2::sessions wiki))
                        sesh))))))

      (let ((expired-sessions
              (make-sesh
               (lambda () (- (get-universal-time)
                             1
                             (random 100000)))))
            (current-sessions
              (make-sesh
               (lambda () (+ (get-universal-time)
                             100
                             (random 100000))))))

        (cliki2::expire-old-sessions wiki)

        (fiveam:is-true
         (notany (lambda (x) (gethash x (cliki2::sessions wiki)))
                 expired-sessions))

        (fiveam:is-true
         (every (lambda (x) (gethash x (cliki2::sessions wiki)))
                current-sessions))))))

(fiveam:test next-expiry-time1
  (fiveam:is (< (+ (* 60 60 24) (get-universal-time))
                (cliki2::next-expiry-time))))

(fiveam:test login1
  (let ((hunchentoot:*reply* (make-instance 'hunchentoot:reply)))
    (cliki2::login
     (list "A Contributor"
           "aol@aol.com"
           (make-string 50)
           (make-string 256)
           nil))
    (let ((secret (hunchentoot:cookie-value
                   (hunchentoot:cookie-out "cliki2auth"))))
      (fiveam:is (= 60 (string-length secret)))
      (fiveam:is (gethash secret
                          (cliki2::sessions cliki2::*wiki*))))))

(fiveam:test account-auth-session-extended
  (let* ((hunchentoot:*reply* (make-instance 'hunchentoot:reply))
         (secret (cliki2::make-random-string 60))
         (old-expiry-time (+ 100 (get-universal-time)))
         (account (cliki2::new-account
                   (cliki2::make-random-string 20)
                   "person2@aol.com"
                   "password1"))
         (session
           (cliki2::make-session
            :account account
            :expires-at old-expiry-time
            :password-digest (cliki2::account-password-digest account))))

    (setf (gethash secret (cliki2::sessions cliki2::*wiki*))
          session)

    (fiveam:is
     (equalp
      account
      (mock 'hunchentoot:cookie-in (constantly secret)
        (cliki2::account-auth))))

    (fiveam:is (< old-expiry-time
                  (cliki2::session-expires-at session)))
    (fiveam:is
     (equalp
      session
      (gethash secret (cliki2::sessions cliki2::*wiki*))))))

(fiveam:test account-auth-cookie-expired
  (let ((hunchentoot:*reply* (make-instance 'hunchentoot:reply))
        (secret (cliki2::make-random-string 60)))
    (setf (gethash secret (cliki2::sessions cliki2::*wiki*))
          (cliki2::make-session :expires-at 3565062000))

    (fiveam:is-false
     (mock 'hunchentoot:cookie-in (constantly secret)
       (cliki2::account-auth)))

    (fiveam:is
     (equal ""
            (hunchentoot:cookie-value
             (hunchentoot:cookie-out "cliki2auth"))))
    (fiveam:is-false
     (gethash secret (cliki2::sessions cliki2::*wiki*)))))

(fiveam:test account-auth-password-changed
  (let ((hunchentoot:*reply* (make-instance 'hunchentoot:reply))
        (secret (cliki2::make-random-string 60)))
    (setf (gethash secret (cliki2::sessions cliki2::*wiki*))
          (cliki2::make-session
           :account (cliki2::new-account
                      (cliki2::make-random-string 20)
                      "person2@aol.com"
                      "password1")
           :expires-at (+ 99999 (get-universal-time))
           :password-digest (make-string 256)))

    (fiveam:is-false
     (mock 'hunchentoot:cookie-in (constantly secret)
       (cliki2::account-auth)))

    (fiveam:is
     (equal ""
            (hunchentoot:cookie-value
             (hunchentoot:cookie-out "cliki2auth"))))
    (fiveam:is-false
     (gethash secret (cliki2::sessions cliki2::*wiki*)))))

(fiveam:test make-captcha1
  ;; also see checks done in contract
  (dotimes (i 10)
    (declare (ignorable i))
    (let ((x (cliki2::make-captcha)))
      (fiveam:is-true (member (first x) '(floor ceiling truncate round)))
      (fiveam:is-true (integerp (second x)))
      (fiveam:is-true (integerp (third x))))))

(test-str emit-captcha-inputs1
  "<input class=\"\" name=\"captcha-answer\" size=\"20\" />
     <input type=\"hidden\" name=\"captcha-op\" value=\"FLOOR\" />
     <input type=\"hidden\" name=\"captcha-x\"  value=\"3\" />
     <input type=\"hidden\" name=\"captcha-y\"  value=\"8\" />"
  (html-output
    (cliki2::emit-captcha-inputs '(floor 3 8) "" 20)))

(fiveam:test check-captcha-fail
  (mock 'hunchentoot:parameter
      (lambda (next x)
        (declare (ignore next))
        (cdr (assoc x '(("captcha-x" . "9")
                        ("captcha-y" . "2")
                        ("captcha-answer" . "3")
                        ("captcha-op" . "truncate"))
                    :test 'equal)))
    (fiveam:is-false (cliki2::check-captcha))))

(fiveam:test check-captcha-pass
  (mock 'hunchentoot:parameter
      (lambda (next x)
        (declare (ignore next))
        (cdr (assoc x '(("captcha-x" . "9")
                        ("captcha-y" . "2")
                        ("captcha-answer" . "4")
                        ("captcha-op" . "truncate"))
                    :test 'equal)))
    (fiveam:is-true (cliki2::check-captcha))))
