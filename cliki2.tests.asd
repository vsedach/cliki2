;;; -*- lisp -*-

(defsystem :cliki2.tests
  :license "AGPL-3.0-or-later"
  :description "Test for CLiki2"
  :components ((:module :tests
                        :serial t
                        :components ((:file "package")
                                     (:file "contracts")
                                     (:file "tests")
                                     (:file "accounts")
                                     (:file "article")
                                     (:file "authentication")
                                     (:file "backlinks")
                                     (:file "diff")
                                     (:file "dispatcher")
                                     (:file "history")
                                     (:file "html-rendering")
                                     (:file "indexes")
                                     (:file "markup")
                                     (:file "recent-changes")
                                     (:file "tools")
                                     (:file "wiki"))))
  :depends-on (:cliki2
               :fiveam :cl-advice :cl-ppcre :uiop :alexandria))
